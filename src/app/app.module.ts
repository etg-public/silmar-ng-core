import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { GeoModule } from '@silmar/ng-core/geo';
import { ThumborModule } from '@silmar/ng-core/thumbor';
import { MdModule } from '@silmar/ng-core/markdown';
import { ValidatorsModule } from '@silmar/ng-core/validators';
import { StringsModule } from '@silmar/ng-core';
import { ColorsModule } from '@silmar/ng-core/colors';
import { ArraysModule } from '@silmar/ng-core';
import { StorageModule } from '@silmar/ng-core/storage';
import { ImageKitModule } from '@silmar/ng-core/imagekit';
import { BusyModule } from '@silmar/ng-core/busy';
import { GoogleMapsModule } from '@angular/google-maps';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { MoneyXrModule } from '@silmar/ng-core/money-xr';


@NgModule({
  declarations : [
    AppComponent
  ],
  imports      : [
    BrowserModule,
    FormsModule,
    CommonModule,
    HttpClientModule,
    HttpClientJsonpModule,
    GoogleMapsModule,
    GeoModule.forRoot({ apiKey : '' }),
    ThumborModule.forRoot({
      imagesDomain    : 'ba-dev.etgr.net',
      thumborEndpoint : 'https://jb.etgr.net',
      supportsWebp    : false
    }),
    ImageKitModule.forRoot({
      endpoint   : 'https://ik.imagekit.io/etg',
      identifier : 'dev'
    }),
    MoneyXrModule.forRoot({ apiUrl : 'https://fixer.etgdev.net/' }),
    MdModule.forRoot(),
    ValidatorsModule,
    StringsModule,
    ColorsModule,
    ArraysModule,
    StorageModule,
    BusyModule.forRoot({ spinner : 'fingerprint' })
  ],
  providers    : [],
  bootstrap    : [ AppComponent ]
})
export class AppModule {
}
