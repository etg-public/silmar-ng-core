import { Component } from '@angular/core';
import { EXAMPLES } from './code_examples';
import { LocalStorage } from '@silmar/ng-core/storage';
import { SessionStorage } from '@silmar/ng-core/storage';
import { CookieStorage } from '@silmar/ng-core/storage';
import { FocusType } from '@silmar/ng-core/imagekit';
import { PushNotificationsService } from '@silmar/ng-core/notifications';
import {Subject, Subscription} from "rxjs";

@Component({
  selector    : 'app-root',
  templateUrl : './app.component.html',
  styles      : [
      `
      label.required:after {
        content: ' *';
      }

      label.required.invalid:after {
        color: #ff3300;
      }

      input.input {
        width: 100px;
      }
    `
  ]
})
export class AppComponent {
  title    = 'Silmar @ NgCore';
  lat      = 48.858837;
  lng      = 2.277020;
  zoom     = 11;
  mode     = 'search';
  place    = 'Eiffel Tower, Paris, France';
  objModel = {
    name  : '',
    age   : null,
    phone : null,
    email : null
  };

  busySubj: Subject<any> = new Subject<any>();
  busy$: Subscription;

  txt = EXAMPLES;

  markdownText = `
# Laeva est est librato oreris longis

## Sua petitam ferebat ac Rutulum rupit iuguloque

Lorem *markdownum* meosque vestem ego *quo signaque*, fore exstantibus vulnere
[loca](http://illetoro.net/), nec, **modo aede** Phaethon. Natas reverentia procul
vulnus niteant demissior agmina!

Auratum modo fecere agris Laertius vulnere vaccae adventus, duobus excussit
ramis. Herbae et surgit ipsum et tumidis bella foresque! Somno demittite laceri
malis. Oscula *cutem tyranni*; causam aequantibus caeli: deploravit intrat
facite. Quod curvos Hesperus satis redeuntem.

## Pestis piscosamque tollit simul tractum libasse fauces

Arma Latonam fronde provida, ut spectans arbitrium [rore
illud](http://www.vidit.io/). Ut in *Pelasgi*, velaturque proque iurgia
**inminet absit** miscent dedit possent. Per id classem inmunis *iaculum*
miserabile plurima parentis visaque nube.

* Cunctae antiquus suasque hominum hoc: huc praeceps nudus ferebat candida
* Cynthia. Tantum teque, thalamos infelix tactis et sonus, cum Cererem ulnis
* sunt cognoscere cepere cladis. Nati ultima expendite, filia iste, si ales
* timidusque Troius!
    `;

  storeKey         = 'foo';
  thWidth: number  = 150;
  thHeight: number = 150;
  ikWidth: number  = 150;
  ikHeight: number = 150;
  ikFocus: FocusType;

  constructor(public localS: LocalStorage,
              public sessS: SessionStorage,
              public cookieS: CookieStorage,
              public pushNotification: PushNotificationsService) {
  }

  filterAdults(item) {
    return item > 10;
  }

  saveLocal(v: string, obj: boolean = false) {
    obj ? this.localS.setObject(this.storeKey, v) : this.localS.setItem(this.storeKey, v);
  }

  removeLocal(all: boolean = false) {
    all ? this.localS.clear() : this.localS.removeItem(this.storeKey);
  }

  saveSess(v: string, obj: boolean = false) {
    obj ? this.sessS.setObject(this.storeKey, v) : this.sessS.setItem(this.storeKey, v);
  }

  removeSess(all: boolean = false) {
    all ? this.sessS.clear() : this.sessS.removeItem(this.storeKey);
  }

  saveCookie(v: string, obj: boolean = false) {
    obj ? this.cookieS.setObject(this.storeKey, v) : this.cookieS.setItem(this.storeKey, v);
  }

  removeCookie(all: boolean = false) {
    all ? this.cookieS.clear() : this.cookieS.removeItem(this.storeKey);
  }

  createPushNotification() {
    this.pushNotification.create('Notification title', {
      body : 'Notification description',
      icon : `assets/images/info.png`
    }).subscribe();
  }

  busyOn() {
    this.busySubj.complete();
    this.busySubj = new Subject();
    this.busy$ = this.busySubj.subscribe();
  }

  busyOff() {
    this.busySubj.complete();
  }
}
