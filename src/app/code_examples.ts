export const EXAMPLES = {
  install       : 'yarn add @silmar/ng-core date-fns',
  arrays_module : `import {ArraysModule} from '@silmar/ng-core';
// ...
@NgModule({
    declarations: [ AppComponent ],
    imports: [
        //...
        ArraysModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}`,
  filter        : `filterAdults(item) {
  return item > 10;
}`,
  filter_use    : '{{[4, 13, 29, 5, 33, 0] | filter:filterAdults}}',
  shuffle_use   : '{{[0, 1, 2, 3, 4, 5, 6, 7, 8, 9] | shuffle}}',

  events_module: `import {EventModifiersModule} from '@silmar/ng-core/event-modifiers
  // ...
@NgModule({
    declarations: [ AppComponent ],
    imports: [
        //...
        EventModifiersModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}`,
  events_stop: `<button (click.stop)="onClick($event, extraData)" [prevent]="true">Click Me!!</button>`,
  events_prevent: `<button (click.prevent)="onClick($event, extraData)" [stop]="true">Click Me!!</button>`,
  events_self: `<div (click.self)="onClick($event, extraData)"><button>Click Me!!</button></div>`,
  events_once: `<button (click.once)="onClick($event, extraData)" [prevent]="true">Click Me!!</button>`,
  events_outside: `<button (click.outside)="onClick($event, extraData)">Click Me!!</button>`,
  busy_module : `import {BusyModule} from '@silmar/ng-core/busy';
// ...
@NgModule({
    declarations: [ AppComponent ],
    imports: [
        //...
        BusyModule.forRoot({
          minDuration        : 100,
          backdrop           : true,
          message            : 'Please wait ',
          wrapperClass       : 'ng-busy',
        })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}`,

  busy_use    : '<div [siBusy]="subscription$"></div>',

  colors_module : `import {ColorsModule} from '@silmar/ng-core/colors';
// ...
@NgModule({
    declarations: [ AppComponent ],
    imports: [
        ColorsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}`,
  colors_use    : `<span [style.background-color]="'first' | hashColor">first</span>
<span [style.background-color]="'second' | hashColor">second</span>
<span [style.background-color]="'third' | hashColor">third</span>
<span [style.background-color]="'first' | hashColor: '100'">first</span>`,

  geo_module       : `import {GeoModule} from '@silmar/ng-core/geo';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    GeoModule.forRoot({apiKey:'YOUR_GMAPS_API_KEY'}) // <-- ADD THIS
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }`,
  geo_flag_install : 'yarn add flag-icons',
  geo_flag_import : '@import "../node_modules/flag-icons/sass/flag-icons.scss";',
  geo_flag_use     : `<si-flag country="de"></si-flag>
<si-flag country="fr" [round]="true"></si-flag>
<si-flag country="gb" [square]="true"></si-flag>`,
  geo_map_install : 'yarn add @angular/google-maps',
  geo_map_module : `
@NgModule({
  imports: [
    ...,
    HttpClientModule,
    HttpClientJsonpModule,
    GoogleMapsModule,
    GeoModule.forRoot({apiKey:'YOUR_GMAPS_API_KEY'}),
    ...
  ],
  ...
})
export class AppModule { }
  `,
  geo_map_embed_use      : '<si-gmaps-embed [lat]="lat" [lng]="lng" [zoom]="zoom" [mode]="mode" [q]="place"></si-gmaps-embed>',
  geo_map_use      : '<google-map *siGmapsLoader></google-map>',
  geo_map_onready_use      : `<ng-template siGmapsLoader (siGmapsLoaderReady)="onMapLoaderReady()">
    <google-map></google-map>
</ng-template>`,
  geo_map_mapready_use      : `<google-map *siGmapsLoader (ready)="onMapReady"></google-map>`,

  md_module     : `import {MdModule} from '@silmar/ng-core/markdown';

@NgModule({
    declarations: [ AppComponent ],
    imports: [
        MdModule.forRoot(<MarkdownConfig>config)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}`,
  md_use_html   : '<div [innerHtml]="markdownText | md"></div>',
  md_use_simple : '<div [innerHtml]="markdownText | md:true"></div>',

  seo_module             : `import {SeoModule} from '@silmar/ng-core/seo';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    SeoModule.forRoot({ga:{isInProdMode: true, trackInNonProd: false}}) // <-- ADD THIS
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }`,
  seo_track              : `gaSvc.pageView('/products', 'Page Title');
// OR
gaSvc.pageView('/products');`,
  seo_event              : `//sendEvent(eventCategory: string, eventAction: string, eventLabel?: string, eventLabel?: string | number)
gaSvc.sendEvent('eventCategory', 'eventAction', 'eventLabel', 'eventLabel');`,
  seo_timing             : `if (window && window.performance) {
  // Gets the number of milliseconds since page load
  // (and rounds the result since the value must be an integer).
  var timeSincePageLoad = Math.round(performance.now());

  // Sends the timing hit to Google Analytics.
  gaSvc.sendTiming('JS Dependencies', 'load', timeSincePageLoad);
}`,
  storage_module         : `import {StorageModule} from '@silmar/ng-core/storage';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    StorageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }`,
  storage_use            : `construct(storage: LocalStorage) {
 storage.setItem('foo', 'bar'); // set a scalar value 'bar' to key 'foo'
 storage.getItem('foo');        // get a scalar value of key 'foo'
 storage.setObject('foo', obj); // set an object/array value to key foo
 storage.getObject('foo');      // get an object/array value
 storage.removeItem('foo');     // remove value with key 'foo'
 storage.key(3);                // returns the name of the nth key in the storage
 storage.clear();               // wipe all values
 storage.length;                // count of keys in storage
}`,
  strings_module         : `import {StringsModule} from '@silmar/ng-core';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    StringsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }`,
  capitalize_use         : '{{\'capitalize pipe\'|capitalize}}',
  capitalize_every_use   : '{{\'capitalize every word pipe\'|capitalize:true}}',
  highlight_use          : '<span [innerHtml]="\'Highlight within text pipe\' | highlight:\'within\'"></span>',
  join_use               : `{{ ['Eiffel Tower', 'Paris', 'France'] | join:', '}}`,
  repeat_pipe_use        : `{{'★' | repeat:5}}`,
  repeat_dir_use         : `<i class="fa fa-star" *siRepeat="5"></i>`,
  substitute_use         : `{{"The quick {color} fox jumps over the lazy {animal}" | substitute:{color:"brown", animal:"dog"} }}`,
  substitute_missing_use : `{{"The quick {color} fox jumps over the lazy {animal}" | substitute:{color:""} }}`,
  trim_use               : `{{' John Doe ' | trim}}`,
  trim_p_use             : `{{'+1234567890 ' | trim:'+'}}`,
  truncate_use           : `{{'Chuck Norris counted to infinity... twice.' | truncate:15}}`,
  truncate_p_use         : `{{'Chuck Norris counted to infinity... twice.' | truncate:3:'...':true}}`,
  attr_map               : `<img siThumbor="expobeds.com/expo_beds_logo.png"
    [mapping]="{src:'data-src', srcset:'data-srcset'}" [width]="150" [height]="150"/>`,
  attr_map_res           : `<img sithumbor="expobeds.com/expo_beds_logo.png"
    data-src="https://jb.etgr.net/unsafe/150x150/ba-dev.etgr.net/expobeds.com/expo_beds_logo.png" />`,
  th_picture             : `<si-thumbor-picture path="expobeds.com/expo_beds_logo.png" [x2]="true" [width]="320" [height]="200"
    [bySize]="{'phone':100,'(max-width: 1060px)':'300x200', 'hd':400, 'fullhd': 600}"></si-thumbor-picture>`,
  ik_picture             : `<si-imagekit-picture path="default-image_6ZRWK2sTe.jpg" [x2]="true" [width]="320" [height]="200"
    [bySize]="{'phone':100,'(max-width: 1060px)':'300x200', 'hd':400, 'fullhd': 600}"></si-imagekit-picture>`,
  ik_transforms          : `<img siImageKit="default-image_6ZRWK2sTe.jpg"
    [transforms]="[{rt:90,bg:'000000'}, {w:180,h:240,c:'at_least'}, {b:'10_FF5000'}, {r:5}]"/>`
};
