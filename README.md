# SilmarNgCore


[![npm (scoped)](https://img.shields.io/npm/v/@silmar/ng-core.svg)](https://www.npmjs.com/package/@silmar/ng-core)
[![pipeline status](https://gitlab.com/etg-public/silmar-ng-core/badges/master/pipeline.svg)](https://gitlab.com/etg-public/silmar-core/commits/master)
[![NPM](https://img.shields.io/npm/l/@silmar/ng-core.svg?style=flat-square)](https://www.npmjs.com/package/@silmar/ng-core)

A bunch of useful Angular 9+ pipes, directives and components (*maybe mainly useful for our company but anyhow*). Feel free to dissect and use as you wish.

We are using parts of [date-fns](https://date-fns.org/) and [flag-icon-css](http://flag-icon-css.lip.is/), so thanks guys.

### Install
```
npm i @silmar/ng-core
// or
yarn add @silmar/ng-core
```

### Documentation
Documentation (kind of...) can be found [here](projects/silmar/ng-core/src/README.md)

### Demo
[Checkout the demo page](https://etg-public.gitlab.io/silmar-ng-core) or run the demo app, check out the project then run the following in th root directory
```
npm i
ng serve
```

### Build
This project uses GitLab CI. In order to build new version do the following steps:

- Update package.json version & commit changes
- Push a tag in format `vX.X.X`
- Wait for the CI build

### Adding a new project

 1. Factor a new library using Angular CLI `ng generate library @silmar/<awesome-lib>`
 1. Change the prefix in `angular.json` file from `lib` to `si`
 1. Add build command in `package.json`.  
 *Example `"build:lib:<awesome-lib>": "yarn run version && ng run @silmar/<awesome-lib>:build"`*
 1. Add new lib to sync version with major one in `package.json` 
 1. Rename `silmar` to `@silmar` in `projects/silmar/<awesome-lib>/ng-package.json` dest folder
