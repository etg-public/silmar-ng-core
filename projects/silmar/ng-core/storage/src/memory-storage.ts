import { Injectable } from '@angular/core';

import { StorageBase } from './storage-base';
import { ServerDriver } from './server-driver';

@Injectable()
export class MemoryStorage extends StorageBase {
  constructor() {
    super(new ServerDriver(), 'MemoryStorage');
  }
}