import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { StorageBase } from './storage-base';
import { ServerDriver } from './server-driver';
import { MemoryStorage } from './memory-storage';

@Injectable()
export class LocalStorage extends StorageBase {
  constructor(@Inject(PLATFORM_ID) platformId: Object) {
    if (isPlatformBrowser(platformId)) {
      super(supportsLocalStorage() ? window.localStorage : new MemoryStorage(), 'LocalStorage');
    } else {
      super(new ServerDriver(), 'LocalStorage');
    }
  }
}

function supportsLocalStorage(): boolean {
  try {
    if (!window || !window.localStorage) {
      return false;
    }

    window.localStorage.setItem('mod', 'mod');
    window.localStorage.removeItem('mod');

    return true;
  } catch (e) {
    return false;
  }
}
