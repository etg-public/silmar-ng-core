export abstract class StorageBase {
  protected storageObject: any;

  protected constructor(storageObject: any, storageObjectName: string) {
    if (!storageObject) {
      throw new Error(`Current browser does not support ${ storageObjectName }`);
    }

    this.storageObject = storageObject;
  }

  getItem(key: string): string {
    return this.storageObject.getItem(key) || null;
  }

  setItem(key: string, value: string): void {
    this._setItemInStorage(key, value);
  }

  removeItem(key: string): void {
    let currentValue = this.tryGetObject(key);

    if (!currentValue) {
      currentValue = this.getItem(key);
    }

    this.storageObject.removeItem(key);
  }

  key(index: number): string {
    return this.storageObject.key(index);
  }

  clear(): void {
    this.storageObject.clear();
  }

  get length(): number {
    return this.storageObject.length;
  }

  getObject<T>(key: string): T {
    let jsonInStorage = this.getItem(key);

    if (jsonInStorage === null) {
      return null;
    }

    return JSON.parse(jsonInStorage) as T;
  }

  tryGetObject<T>(key: string): T {
    try {
      return this.getObject<T>(key);
    } catch (e) {
      return null;
    }
  }

  setObject(key: string, value: any): void {
    this._setItemInStorage(key, JSON.stringify(value));
  }

  protected _setItemInStorage(key: string, value: string) {
    this.storageObject.setItem(key, value);
  }
}