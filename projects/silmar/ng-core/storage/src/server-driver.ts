import { Driver } from "./driver";

export class ServerDriver implements Driver {
  private _storageObject: any;

  constructor() {
    this._storageObject = {};
  }

  getItem(key: string): string {
    return this._storageObject[key] || null;
  }

  setItem(key: string, value: string): void {
    this._storageObject[key] = value;
  }

  removeItem(key: string): void {
    this._storageObject[key] = undefined;
  }

  key(index: number): string {
    return this._storageObject.key(index);
  }

  clear(): void {
    this._storageObject = {};
  }

  get length(): number {
    return Object.keys(this._storageObject).length;
  }
}