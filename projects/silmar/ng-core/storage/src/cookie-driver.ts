import { Driver } from "./driver";
import { isDefined } from "@silmar/ng-core";

export class CookieDriver implements Driver {
  constructor() {
    if (!isDefined(document.cookie)) {
      throw new Error(`Current browser does not support cookies`);
    }
  }

  getItem(key: string): string {
    let cookieExists = CookieDriver.exists(key);

    if (cookieExists) {
      key = encodeURIComponent(key);

      let regexp  = new RegExp('(?:^' + key + '|;\\s*' + key + ')=(.*?)(?:;|$)', 'g');
      let cookies = regexp.exec(document.cookie);

      return decodeURIComponent(cookies[ 1 ]);
    }
    else {
      return '';
    }
  }

  setItem(key: string, value: string, validity?: number, validityType?: string, domain?: string, path?: string, needsSecureConnection?: boolean): void {
    let cookieStr = encodeURIComponent(key) + '=' + encodeURIComponent(value) + ';';

    /**
     * Sets validity of cookie
     */
    if (validity) {

      let secondsValidity = validity * 1000;

      if (validityType == 'minutes') {
        secondsValidity *= 60;
      }
      else if (validityType == 'hours') {
        secondsValidity *= (60 * 60);
      }
      else if (validityType == 'days') {
        secondsValidity *= (60 * 60 * 24);
      }

      let daysValid = new Date(new Date().getTime() + secondsValidity);

      cookieStr += 'expires=' + daysValid.toUTCString() + ';';
    }

    if (path) {
      cookieStr += 'path=' + path + ';';
    }

    if (domain) {
      cookieStr += 'domain=' + domain + ';';
    }

    if (needsSecureConnection) {
      cookieStr += 'secure;';
    }

    document.cookie = cookieStr;
  }

  removeItem(key: string, domain?: string, path?: string): void {
    let cookieExists = CookieDriver.exists(key);

    // If the cookie exists
    if (cookieExists) {
      this.setItem(key, '', -1, '', domain, path);
    }
  }

  key(index: number): string {
    let keyVal = document.cookie.split(";")[ index ] || '';

    return keyVal ? keyVal.split('=')[0] : '';
  }

  clear(): void {
    document.cookie.split(";")
      .forEach(c => document.cookie = c.replace(/^ +/, "")
        .replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"));
  }

  get length(): number {
    const cc = document.cookie;

    return cc ? document.cookie.split(";").length : 0;
  }

  protected static exists(cookieName: string): boolean {

    cookieName = encodeURIComponent(cookieName);

    let regexp = new RegExp('(?:^' + cookieName + '|;\\s*' + cookieName + ')=(.*?)(?:;|$)', 'g');

    return regexp.test(document.cookie);
  }
}

