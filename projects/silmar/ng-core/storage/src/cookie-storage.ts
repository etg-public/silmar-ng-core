import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { StorageBase } from './storage-base';
import { ServerDriver } from './server-driver';
import { CookieDriver } from "./cookie-driver";

@Injectable()
export class CookieStorage extends StorageBase {
  constructor(@Inject(PLATFORM_ID) platformId: Object) {
    if (isPlatformBrowser(platformId)) {
      super(new CookieDriver(), 'CookieStorage');
    } else {
      super(new ServerDriver(), 'CookieStorage');
    }
  }

  removeItem(key: string, domain?: string, path?: string): void {
    let currentValue = this.tryGetObject(key);

    if (!currentValue) {
      currentValue = this.getItem(key);
    }

    this.storageObject.removeItem(key, domain, path);
  }

  setObject(key: string, value: any, validity?: number, validityType?: string, domain?: string, path?: string, needsSecureConnection?: boolean): void {
    this._setItemInStorage(key, JSON.stringify(value), validity, validityType, domain, path, needsSecureConnection);
  }

  setItem(key: string, value: string, validity?: number, validityType?: string, domain?: string, path?: string, needsSecureConnection?: boolean): void {
    this._setItemInStorage(key, value, validity, validityType, domain, path, needsSecureConnection);
  }

  get length(): number {
    return this.storageObject.length;
  }

  protected _setItemInStorage(key: string, value: string, validity?: number, validityType?: string, domain?: string, path?: string, needsSecureConnection?: boolean): void {
    this.storageObject.setItem(key, value, validity, validityType, domain, path, needsSecureConnection);
  }
}
