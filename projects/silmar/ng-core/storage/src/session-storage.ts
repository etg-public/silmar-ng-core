import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { StorageBase } from './storage-base';
import { ServerDriver } from './server-driver';

@Injectable()
export class SessionStorage extends StorageBase {
  constructor(@Inject(PLATFORM_ID) platformId: Object) {
    if (isPlatformBrowser(platformId)) {
      super(window.sessionStorage, 'SessionStorage');
    } else {
      super(new ServerDriver(), 'SessionStorage');
    }
  }
}