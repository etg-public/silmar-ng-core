import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { CookieStorage } from "./cookie-storage";
import { LocalStorage } from "./local-storage";
import { SessionStorage } from "./session-storage";
import { MemoryStorage } from "./memory-storage";

@NgModule({
  imports   : [ CommonModule ],
  providers : [ CookieStorage, LocalStorage, SessionStorage, MemoryStorage ],
})
export class StorageModule {
}