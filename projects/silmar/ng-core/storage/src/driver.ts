export interface Driver {
  getItem(key: string): string;

  setItem(key: string, value: string): void;

  removeItem(key: string): void;

  key(index: number): string;

  clear(): void;
}