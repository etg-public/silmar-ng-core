export * from './cookie-storage';
export * from './local-storage';
export * from './memory-storage';
export * from './session-storage';
export * from './storage.module';