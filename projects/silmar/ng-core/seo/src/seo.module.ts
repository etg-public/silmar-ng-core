import { ModuleWithProviders, NgModule } from '@angular/core';
import { SEO_CONFIG, SeoConfig } from "./config";
import { CommonModule } from "@angular/common";
import { GoogleAnalyticsService } from "./google-analytics.service";

@NgModule({
  imports      : [ CommonModule ],
  declarations : [],
  exports      : []
})
export class SeoModule {
  static forRoot(seoConfig: SeoConfig): ModuleWithProviders<SeoModule> {
    return {
      ngModule  : SeoModule,
      providers : [
        { provide : SEO_CONFIG, useValue : seoConfig },
        GoogleAnalyticsService
      ]
    };
  }
}