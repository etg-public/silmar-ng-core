import { InjectionToken } from '@angular/core';

export const SEO_CONFIG = new InjectionToken<SeoConfig>('silmar-seo-module SEO_CONFIG');

/**
 *
 */
export interface SeoConfig {
  ga: {
    isInProdMode: boolean;
    trackInNonProd: boolean;
    ga4?: boolean;
    ga?: boolean;
    piwik?: boolean;
  }
}
