import { SEO_ANALYTICS_EVENTS } from './events';

export const PIWIK_METHOD_EVENT_MAP = {
  [ SEO_ANALYTICS_EVENTS.add_to_cart ]    : 'addEcommerceItem',
  [ SEO_ANALYTICS_EVENTS.begin_checkout ] : 'trackEcommerceCartUpdate',
  [ SEO_ANALYTICS_EVENTS.purchase ]       : 'trackEcommerceOrder',
  [ SEO_ANALYTICS_EVENTS.view_item ]      : 'setEcommerceView',
  [ SEO_ANALYTICS_EVENTS.search ]         : 'trackSiteSearch',
};
