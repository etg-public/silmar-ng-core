import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { GASendObject } from './ga-send-object';
import { SEO_CONFIG, SeoConfig } from './config';
import { isPlatformBrowser } from '@angular/common';
import { GA4_EVENTS } from './ga4_events';
import { ShoppingCardType } from './types';
import { SEO_ANALYTICS_EVENTS } from './events';
import { PIWIK_METHOD_EVENT_MAP } from './mapper';


declare var _paq: any[];

@Injectable()
export class AnalyticsService {

  /**
   *
   * @param cfg
   * @param platformId
   */
  constructor(@Inject(SEO_CONFIG) protected cfg: SeoConfig, @Inject(PLATFORM_ID) platformId) {
    this.isBrowser = isPlatformBrowser(platformId);
    this.setDevMode();
  }

  protected static isDebug: boolean;
  /**
   *
   */
  protected isBrowser: boolean;

  protected origHit: any;

  protected static gaSend(details: GASendObject) {
    ga('send', details);
  }

  protected static gtagSendEvent(eventName: string, eventParams: { [ key: string ]: any }) {
    if (this.isDebug) {
      eventParams.debug_mode = true;
    }

    gtag('event', eventName, eventParams);
  }

  protected static piwikSendEvent(eventName: string, eventCategory?: string, eventLabel?: string, eventValue?: number | string, params?: any) {
    if (!this.isDebug) {
      _paq.push([
        'trackEvent',
        eventCategory,
        eventName,
        eventLabel,
        eventValue ]);

      if (PIWIK_METHOD_EVENT_MAP.hasOwnProperty(eventName)) {
        let prop = 'piwik_' + PIWIK_METHOD_EVENT_MAP[ eventName ];
        AnalyticsService[ prop ](eventLabel, eventValue, eventCategory, params);
      }
    }
  }

  protected static piwik_trackSiteSearch(ignore: any, keyword: any, category: string, params?: any) {
    if (!this.isDebug) {
      _paq.push([ PIWIK_METHOD_EVENT_MAP[ SEO_ANALYTICS_EVENTS.search ], keyword, category, params?.result_count ]);
    }
  }

  protected static piwik_setEcommerceView(item: string, value: number, category?: string, params?: any) {
    if (!this.isDebug) {
      _paq.push([
        PIWIK_METHOD_EVENT_MAP[ SEO_ANALYTICS_EVENTS.view_item ],
        params?.items[ 0 ]?.item_id || item.toLowerCase().replace(' ', '_'),
        item,
        category,
        value
      ]);
    }
  }

  protected static piwik_addEcommerceItem(item: string, value: number, category?: string, params?: any) {
    if (!this.isDebug) {
      _paq.push([ 'clearEcommerceCart' ]);
      _paq.push([
        PIWIK_METHOD_EVENT_MAP[ SEO_ANALYTICS_EVENTS.add_to_cart ],
        params?.items[ 0 ]?.item_id || item.toLowerCase().replace(' ', '_'),
        item,
        category,
        value,
        1
      ]);
    }
  }

  protected static piwik_trackEcommerceCartUpdate(item: string, value: number, category?: string) {
    if (!this.isDebug) {
      _paq.push([ PIWIK_METHOD_EVENT_MAP[ SEO_ANALYTICS_EVENTS.begin_checkout ], value ]);
    }
  }

  protected static piwik_trackEcommerceOrder(item: string, value: number, category?: string, params?: any) {
    if (!this.isDebug) {
      const order = (params as ShoppingCardType)?.items ? params.items[ 0 ]?.item_id : undefined;

      _paq.push([ PIWIK_METHOD_EVENT_MAP[ SEO_ANALYTICS_EVENTS.purchase ], order || 'Booking_' + Math.random(), value ]);
    }
  }

  /**
   * Send a page view (PIWIK tracks this automatically)
   *
   * @param url
   * @param title
   * @param fullHref
   */
  pageView(url: string, title?: string, fullHref?: string) {
    if (!this.canAnalyze()) {
      return;
    }

    if (this.gtagEnabled()) {
      AnalyticsService.gtagSendEvent(GA4_EVENTS.page_view, {
        page_title    : title || '',
        page_location : fullHref ? fullHref : window?.location?.href,
        page_path     : url,
      });
    }

    if (this.gaEnabled()) {
      title && ga('set', 'title', title);
      ga('set', 'page', url);
      AnalyticsService.gaSend({ hitType : 'pageview' });
    }
  }

  addToCard(category: string, item: string, total: number, params?: ShoppingCardType) {
    this.sendEvent(category, SEO_ANALYTICS_EVENTS.add_to_cart, item, total, params);
  }

  beginCheckout(category: string, item: string, total: number, params?: ShoppingCardType) {
    this.sendEvent(category, SEO_ANALYTICS_EVENTS.begin_checkout, item, total, params);
  }

  purchase(category: string, item: string, total: number, params?: ShoppingCardType) {
    this.sendEvent(category, SEO_ANALYTICS_EVENTS.purchase, item, total, params);
  }

  /**
   * Send an event to analytics
   *
   */
  sendEvent(category: string, event: string, label?: string, value?: string | number, customParams?: { [ key: string ]: any }) {
    if (!this.canAnalyze()) {
      return;
    }

    if (this.gtagEnabled()) {
      const params = customParams ? customParams : {
        event_category : category,
        event_label    : label || null,
        value          : value || null
      };

      AnalyticsService.gtagSendEvent(event, params);
    }

    if (this.gaEnabled()) {
      AnalyticsService.gaSend({
        hitType       : 'event',
        eventCategory : category,
        eventAction   : event,
        eventLabel    : label,
        eventValue    : value
      });
    }

    if (this.piwikEnabled()) {
      AnalyticsService.piwikSendEvent(event, category, label, value, customParams);
    }
  }

  /**
   * Send timing event to GA
   *
   * @param category
   * @param name
   * @param value
   * @param label
   */
  sendTiming(category: string, name: string, value: string | number, label?: string) {
    if (!this.canAnalyze()) {
      return;
    }

    if (this.gtagEnabled()) {
      AnalyticsService.gtagSendEvent('timing_complete', {
        name           : name,
        value          : value,
        event_category : category,
        event_label    : label || null
      });
    }

    if (this.gaEnabled()) {
      AnalyticsService.gaSend({
        hitType        : 'timing',
        timingCategory : category,
        timingAction   : name,
        timingValue    : value,
        timingLabel    : label
      });
    }
  }

  /**
   * Patch the sendHitTask in dev mode
   */
  protected setDevMode() {
    if (!this.isBrowser) {
      return;
    }

    if (!this.cfg.ga.isInProdMode && !this.cfg.ga.trackInNonProd) {
      if (this.gaEnabled()) {
        this.origHit = ga('get', 'sendHitTask');

        ga('set', 'sendHitTask', model => {
          // tslint:disable-next-line:no-console
          console.debug(model.get('hitPayload'));
        });
      }

      AnalyticsService.isDebug = true;
    } else {
      if (this.origHit) {
        ga('set', 'sendHitTask', this.origHit);
        this.origHit = undefined;
      }

      AnalyticsService.isDebug = false;
    }
  }

  /**
   *
   */
  protected canAnalyze() {
    if (!this.isBrowser) {
      return false;
    }

    return (this.gtagEnabled() || this.gaEnabled() || this.piwikEnabled()) && true;
  }

  protected gtagEnabled() {
    return this.cfg.ga.ga4 && (typeof gtag != 'undefined') && true;
  }

  protected gaEnabled() {
    return this.cfg.ga.ga && (typeof ga != 'undefined') && true;
  }

  protected piwikEnabled() {
    return this.cfg.ga.piwik && (typeof _paq != 'undefined') && true;
  }
}
