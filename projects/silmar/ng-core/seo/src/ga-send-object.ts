export interface GASendObject {
  hitType: 'pageview' | 'screenview' | 'event' | 'transaction' | 'item' | 'social' | 'exception' | 'timing';
  eventCategory?: string;
  eventAction?: string;
  eventLabel?: string;
  eventValue?: string | number;
  timingCategory?: string;
  timingAction?: string;
  timingLabel?: string;
  timingValue?: string | number;
}