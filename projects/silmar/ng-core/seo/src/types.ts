export type ShoppingCardType = {
  currency: string,
  value: number,
  transaction_id?: string,
  items?: {
    item_id: any,
    item_name: string,
    item_category: string | number
  }[]
}
