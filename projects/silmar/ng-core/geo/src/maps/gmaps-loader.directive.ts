import {
  ChangeDetectorRef,
  Directive,
  EventEmitter,
  Inject,
  Output, PLATFORM_ID,
  Renderer2,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { GEO_API_CONFIG, GeoApiConfigLiteral } from './config';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Loader } from '@googlemaps/js-api-loader';


// const API_ROOT           = 'https://maps.googleapis.com/maps/api/js';
const API_ROOT_CLUSTERER = 'https://unpkg.com/@googlemaps/markerclusterer/dist/index.min.js';

@Directive({
  selector : '[siGmapsLoader]',
})
export class GmapsLoaderDirective {
  @Output() siGmapsLoaderReady: EventEmitter<boolean> = new EventEmitter<boolean>();

  static loading: boolean  = false;
  static isLoaded: boolean = false;

  constructor(@Inject(GEO_API_CONFIG) config: GeoApiConfigLiteral,
              protected cd: ChangeDetectorRef,
              @Inject(DOCUMENT) protected document: any,
              @Inject(PLATFORM_ID) platformId,
              protected renderer: Renderer2,
              protected viewContainer: ViewContainerRef,
              protected templateRef: TemplateRef<any>) {
    if (isPlatformBrowser(platformId)) {
      this.load(config);
    }
  }

  protected load(config: GeoApiConfigLiteral) {
    if (GmapsLoaderDirective.loading || GmapsLoaderDirective.isLoaded) {
      GmapsLoaderDirective.isLoaded &&  (!GmapsLoaderDirective.loading) && this.show();

      return;
    }

    GmapsLoaderDirective.loading = true;

    const loader         = new Loader({ apiKey : config.apiKey, version : 'weekly' });
    const markerCluster$ = config.markerClusterer ? this.loadMarkerCluster() : new Promise<boolean>((resolve) => resolve(true));

    Promise.all([ loader.importLibrary('maps'), markerCluster$ ])
      .then(() => {
        GmapsLoaderDirective.isLoaded = true;
        this.siGmapsLoaderReady.emit(true);
        this.show();
      })
      .catch((reason) => {
        console.error('Error loading Google Maps API', reason);
        GmapsLoaderDirective.isLoaded = false;
        this.noShow();
      })
      .finally(() => {
        GmapsLoaderDirective.loading = false;
      });
  }

  protected show() {
    this.viewContainer.clear();
    this.viewContainer.createEmbeddedView(this.templateRef);
    this.cd.markForCheck();
  }

  protected noShow() {
    this.viewContainer.clear();
    this.cd.markForCheck();
  }

  protected loadMarkerCluster() {
    return new Promise<boolean>((resolve, reject) => {
      const scriptElt = this.renderer.createElement('script');
      this.renderer.setAttribute(scriptElt, 'type', 'text/javascript');
      this.renderer.setAttribute(scriptElt, 'src', API_ROOT_CLUSTERER);

      scriptElt.onload  = () => resolve(true);
      scriptElt.onerror = () => resolve(false);

      this.renderer.appendChild(this.document.head, scriptElt);
    });
  }
}
