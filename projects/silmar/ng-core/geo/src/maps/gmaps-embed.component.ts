import {
  Component,
  Inject,
  Input,
  SimpleChanges,
  OnChanges,
  ChangeDetectionStrategy,
  SecurityContext
} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {ucfirst} from '@silmar/ng-core';
import {GEO_API_CONFIG, GeoApiConfigLiteral} from './config';

const API_ROOT = 'https://www.google.com/maps/embed/v1/';

/**
 * Google Maps component for Google Maps Embed API
 * @link: https://developers.google.com/maps/documentation/embed/guide
 */
@Component({
  selector: 'si-gmaps-embed',
  template: `
    <iframe style="width: 100%; height: 100%; border: none;" [src]="request" allowfullscreen></iframe>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [ ':host {display: block}' ],
  host: {'[style]': 'style'}
})
export class GmapsEmbedComponent implements OnChanges {
  /**
   * API key
   */
  apiKey: string;

  /**
   * @type {string}
   */
  @Input() mode: 'place' | 'directions' | 'search' = 'place';

  /**
   * Inline style of the component
   */
  @Input() style: any = { width : '100%', height : '300px' };

  // Map options.
  // @see: https://developers.google.com/maps/documentation/embed/guide#map_modes
  @Input() q: string = 'Varna';
  @Input() origin: string;
  @Input() destination: string;
  @Input() waypoints: string[] | string;
  @Input() directionsMode: 'driving' | 'walking' | 'bicycling' | 'transit' | 'flying';
  @Input() avoid: string[] | string;
  @Input() lat: number;
  @Input() lng: number;
  @Input() zoom: number;

  /**
   * Request url in format
   * https://www.google.com/maps/embed/v1/{MODE}?key={YOUR_API_KEY}&{parameters}
   */
  protected _request: string;

  /**
   *
   */
  constructor(@Inject(GEO_API_CONFIG) config: GeoApiConfigLiteral, private _sanitizer: DomSanitizer) {
    this.apiKey = config.apiKey;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.buildRequest();
  }

  get request(): SafeResourceUrl {
    return this._sanitizer.bypassSecurityTrustResourceUrl(this._request);
  }

  buildRequest() {
    this._request = this._sanitizer.sanitize(SecurityContext.URL, this['buildFor' + ucfirst(this.mode)]());
  }

  protected buildForPlace() {
    let url = this.endpoint() + '&q=' + encodeURIComponent(this.q);

    this.zoom && (url += `&zoom=${this.zoom}`);

    return url;
  }

  protected buildForSearch() {
    let url = this.endpoint() + '&q=' + encodeURIComponent(this.q);

    this.lat && this.lng && (url += `&center=${this.lat},${this.lng}`);
    this.zoom && (url += `&zoom=${this.zoom}`);

    return url;
  }

  protected buildForDirections() {
    let url = this.endpoint() +
      `&origin=${encodeURIComponent(this.origin)}&destination=${encodeURIComponent(this.destination)}`;

    if (this.waypoints) {
      let arr: Array<string> = this.waypoints.constructor === Array ? <string[]>this.waypoints : [<string>this.waypoints];
      url += '&waypoints=' + arr.map(item => encodeURIComponent(item)).join('|');
    }

    if (this.avoid) {
      let arr: Array<string> = this.avoid.constructor === Array ? <string[]>this.avoid : [<string>this.avoid];
      url += '&avoid=' + arr.map(item => encodeURIComponent(item)).join('|');
    }

    this.directionsMode && (url += '&mode=' + encodeURIComponent(this.directionsMode));

    return url;
  }

  protected endpoint() {
    return API_ROOT + this.mode + '?key=' + this.apiKey;
  }
}
