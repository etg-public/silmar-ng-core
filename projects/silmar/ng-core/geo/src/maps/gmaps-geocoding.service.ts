import { Injectable, Inject, Optional } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { LogService } from '@silmar/ng-core';
import { GEO_API_CONFIG, GeoApiConfigLiteral } from './config';
import { catchError, map } from "rxjs/operators";
import { HttpClient, HttpParams } from '@angular/common/http';

const API_ROOT     = 'https://maps.googleapis.com/maps/api/geocode/json';
const API_STATUSES = {
  'OK'               : 'No errors occurred',
  'ZERO_RESULTS'     : 'No results found',
  'OVER_QUERY_LIMIT' : 'Quota limit exceeded',
  'REQUEST_DENIED'   : 'Your request was denied',
  'INVALID_REQUEST'  : 'Address, components or lat-lng is missing',
  'UNKNOWN_ERROR'    : 'The request could not be processed due to a server error'
};

/**
 * Geocoding service
 */
@Injectable({providedIn: 'root'})
export class GmapsGeocodingService {
  /**
   * API key
   */
  protected apiKey: string;

  /**
   * @param  logger
   * @param  http
   * @param  config
   */
  constructor(@Optional() protected logger: LogService, protected http: HttpClient, @Inject(GEO_API_CONFIG) config: GeoApiConfigLiteral) {
    if (!(this.apiKey = config.apiKey)) {
      this.error('Cannot use geocoding service without API key!');
    }
  }

  /**
   * Geocode address string
   * @param address
   * @param language
   * @param region
   */
  geocode(address: string, language: string = null, region: string = null): Observable<any> {
    return this.http.get(API_ROOT, this.buildReqArgs(address, language, region))
      .pipe(
        map((res: { status: any, results: { [ key: string ]: any }[] }) => {
          let data = res;

          if (!data.status || data.status != 'OK') {
            throw new Error(API_STATUSES[ data.status ] || 'Geocoding error');
          }

          return this.normalize(res.results[ 0 ].geometry.location);
        }),
        catchError((error) => {
          this.error(error.message);

          return throwError(error);
        })
      );
  }

  /**
   * @param address
   * @param language
   * @param region
   */
  protected buildReqArgs(address: string, language: string, region: string): {} {
    let params: HttpParams = new HttpParams({
      fromObject : {
        key     : this.apiKey,
        address
      }
    });

    language && (params = params.set('language', language));
    region && (params = params.set('region', region));

    return { params };
  }

  /**
   * @param err
   */
  protected error(err: string) {
    this.logger ? this.logger.to('geocoding').error(err) : console.error(err);
  }

  protected normalize(location: any) {
    if (!location.lat || !location.lng) {
      throw new Error('Bad response');
    }

    location.lat = Number(location.lat).toFixed(6);
    location.lng = Number(location.lng).toFixed(6);

    return location;
  }
}
