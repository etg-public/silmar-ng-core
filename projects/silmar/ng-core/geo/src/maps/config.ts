import {InjectionToken} from '@angular/core';

export const GEO_API_CONFIG = new InjectionToken<GeoApiConfigLiteral>('silmar-geo-module GEO_API_CONFIG');

export interface GeoApiConfigLiteral {
    /**
     * The Google Maps API Key (see:
     * https://developers.google.com/maps/documentation/javascript/get-api-key)
     */
    apiKey?: string;
    markerClusterer?: boolean;
}
