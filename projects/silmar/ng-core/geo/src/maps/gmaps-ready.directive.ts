import {Directive, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import { GoogleMap } from '@angular/google-maps';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';

/**
 * Extends google-map component with (ready) event
 */
@Directive({ selector : 'google-map[ready]' })
export class GmapsReadyDirective implements OnInit, OnDestroy {
  @Output('ready') initEvent = new EventEmitter();

  protected subscription: Subscription;

  constructor(protected gMap: GoogleMap) {
  }

  ngOnInit() {
    this.subscription = this.gMap.idle.pipe(first())
      .subscribe(event => this.initEvent.emit(event));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }
}
