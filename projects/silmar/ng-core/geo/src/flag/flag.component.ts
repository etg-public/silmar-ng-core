import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

const ALIASES = { en : 'gb' };

@Component({
  selector : 'si-flag',
  template : '',
  styles   : [ `
    :host { display: inline-block; }
    :host.fir { border-radius: 50% }
  ` ],
  changeDetection : ChangeDetectionStrategy.OnPush,
  host            : {
    '[class]'           : 'getClasses',
    '[attr.title]'      : 'title',
    '[style.width.px]'  : 'width',
    '[style.height.px]' : 'getHeight'
  }
})
export class FlagComponent {
  @Input() width: number          = 32;
  @Input() square: boolean        = false;
  @Input() round: boolean         = false;
  @Input() useBackground: boolean = false;
  @Input() title: string;

  protected _country: string;
  protected _currency: string;

  get getClasses() {
    let classes = { 'dib' : true, 'fi' : !this.useBackground, 'fib' : this.useBackground };

    if (this._country) {
      classes[ 'fi-' + this._country ] = true;
    } else if (this._currency) {
      classes[ 'fi-' + this._currency ] = true;
    }

    (this.square || this.round) && (classes[ 'fis' ] = true);
    this.round && (classes[ 'fir' ] = true);

    return classes;
  }

  /**
   * ISO 2-letter country code
   * @param value
   */
  @Input()
  set country(value: string) {
    this._country = value ? value.toLowerCase() : '';

    ALIASES.hasOwnProperty(this._country) && (this._country = ALIASES[ this._country ]);
  }
  @Input()
  set currency(value: string) {
    this.country = value.substring(0, 2);
  }

  get country() {
    return this._country;
  }

  get getHeight() {
    return this.width / (this.square || this.round ? 1 : (1.33333333));
  }
}
