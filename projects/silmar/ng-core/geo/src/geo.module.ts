import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FlagComponent} from './flag/flag.component';
import {GEO_API_CONFIG, GeoApiConfigLiteral} from './maps/config';
import {GmapsEmbedComponent} from './maps/gmaps-embed.component';
import {GmapsLoaderDirective} from "./maps/gmaps-loader.directive";
import {GmapsReadyDirective} from "./maps/gmaps-ready.directive";


const declarables = [FlagComponent, GmapsEmbedComponent, GmapsLoaderDirective, GmapsReadyDirective];

@NgModule({
  imports: [CommonModule],
  declarations: [declarables],
  exports: [declarables]
})
export class GeoModule {
  static forRoot(geoApiConfig?: GeoApiConfigLiteral): ModuleWithProviders<GeoModule> {
    return {
      ngModule: GeoModule,
      providers: [{provide: GEO_API_CONFIG, useValue: geoApiConfig}]
    };
  }
}
