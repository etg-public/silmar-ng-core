import { InjectionToken } from '@angular/core';

export const MONEY_XR_CONFIG = new InjectionToken<MoneyXrConfig>('silmar-money-xr MONEY_XR_CONFIG');

export interface MoneyXrConfig {
  apiUrl: string;
  baseCurrency?: string;
  accessKey?: string;
}
