import { Inject, Injectable } from '@angular/core';
import { MONEY_XR_CONFIG, MoneyXrConfig } from './config';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { FixerData } from './fixer-data';
import { differenceInMinutes, format } from 'date-fns';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ConversionData } from './conversion-data';
import { MoneyXrConvert } from './money-xr-convert';

const ESSENTIAL_CURRENCIES = [ 'EUR', 'USD', 'GBP', 'CAD', 'CHF', 'NOK', 'SEK', 'BGN', 'RUB', 'JPY', 'CNY', 'INR', ];

@Injectable({
  providedIn : 'root'
})
export class MoneyXrService {
  /**
   * Base currency
   * @type {string}
   */
  readonly base: string = 'EUR';

  /**
   * URL of the API that will return exchange rates
   * @type {string}
   */
  protected apiUrl: string = 'https://api.exchangerate.host/';

  /**
   *
   * @protected
   */
  protected accessKey: string;

  /**
   * in minutes
   * @protected
   */
  protected ratesCacheTime = 120;

  /**
   *
   * @protected
   */
  protected fixerData: null | FixerData;

  /**
   *
   * @protected
   */
  protected fixerTime: Date;

  /**
   *
   * @protected
   */
  protected latest$: Subject<FixerData>;

  constructor(protected http: HttpClient, @Inject(MONEY_XR_CONFIG) config: MoneyXrConfig) {
    this.apiUrl    = config.apiUrl;
    this.accessKey = config.accessKey;
    this.base      = config.baseCurrency || this.base;

    if (!this.accessKey) {
      console.warn('No access key for money XR service');
    }
  }

  /**
   * Get latest exchange rates
   */
  latest(): Observable<FixerData> {
    if (this.fixerData && differenceInMinutes(new Date(), this.fixerTime) < this.ratesCacheTime) {
      return of(this.fixerData);
    }

    if (this.latest$ && !this.latest$.isStopped) {
      return this.latest$;
    }

    this.latest$ = new Subject<FixerData>();

    this.doRequest('latest', { base : this.base })
      .subscribe(
        fixer => {
          this.fixerData = fixer;
          this.fixerTime = new Date();

          this.latest$.next(this.fixerData);
          this.latest$.complete();
        },
        error => {
          this.handleError(error);
          this.latest$.error(error);
        }
      );

    return this.latest$;
  }

  /**
   * Get historical exchange rates
   */
  historical(date: string | Date): Observable<FixerData> {
    if (date instanceof Date) {
      date = format(date, 'yyyy-MM-dd');
    }

    return this.doRequest(date, { base : this.base }).pipe(catchError(err => this.handleError(err)));
  }

  /**
   * Get latest exchange rates for essential currencies
   *
   * @return {Observable<FixerData>}
   */
  latestEssential(): Observable<FixerData> {
    return this.latest()
      .pipe(
        map(d => {
          d.rates = Object.keys(d.rates)
            .filter(k => ESSENTIAL_CURRENCIES.includes(k))
            .reduce((obj, key) => {
              obj[ key ] = d.rates[ key ];

              return obj;
            }, {});

          return d;
        })
      );
  }

  /**
   * Can we convert the given currency
   *
   * @param currency
   */
  canConvert(currency: string): Observable<boolean> {
    return this.allowedRates().pipe(map((rates) => rates.indexOf(currency.toUpperCase()) > -1));
  }

  /**
   * Get all known currencies
   */
  allowedRates(): Observable<string[]> {
    return this.latest()
      .pipe(
        map((fixer: FixerData) => {
          let res = Object.keys(fixer?.rates || {});
          !fixer.rates[ fixer.base ] && res.push(fixer.base);
          res.sort();

          return res;
        })
      );
  }

  /**
   * Convert X amount of base (from) currency to another currency
   */
  convert(from: string, to: string, amount: number | string = 1, date: string = null, fixedFloat: number = 4): Observable<ConversionData> {
    if (from == to) {
      return of({
        base : from, to : to, date : date || (new Date() + ''), amount : parseFloat(amount + '').toFixed(fixedFloat),
      });
    }

    return (date ? this.historical(date) : this.latest())
      .pipe(
        map((fixer: FixerData) => {
          const rateFixed = ((new MoneyXrConvert(fixer.base, fixer.rates)).convert(amount, from.toUpperCase(), to.toUpperCase()) || 0).toFixed(fixedFloat);

          return {
            base   : fixer.base,
            to     : to,
            date   : fixer.date,
            amount : rateFixed,
          };
        })
      );
  }

  /**
   * Request from API exchange rates
   *
   * @param path
   * @param params
   */
  protected doRequest(path, params?: { [ key: string ]: any }): Observable<FixerData> {
    if (!params) {
      params = {};
    }

    if (this.accessKey) {
      params[ 'access_key' ] = this.accessKey;
    }

    return this.http.get(this.apiUrl + path, { params : params }) as Observable<FixerData>;
  }

  /**
   * Handle API error
   *
   * @param error
   */
  protected handleError(error: HttpResponse<any>): Observable<any> {
    console.error(error);

    return throwError(error);
  }
}
