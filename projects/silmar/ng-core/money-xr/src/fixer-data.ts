export class FixerData {
  base: string;
  date: string;
  rates: { [ currency: string ]: number };
}
