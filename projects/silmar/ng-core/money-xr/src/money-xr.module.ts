import { ModuleWithProviders, NgModule } from '@angular/core';
import { MONEY_XR_CONFIG, MoneyXrConfig } from './config';
import { MoneyXrPipe } from './money-xr.pipe';

@NgModule({
  declarations : [ MoneyXrPipe ],
  exports      : [ MoneyXrPipe ]
})
export class MoneyXrModule {
  static forRoot(config: MoneyXrConfig): ModuleWithProviders<MoneyXrModule> {
    return {
      ngModule  : MoneyXrModule,
      providers : [ { provide : MONEY_XR_CONFIG, useValue : config } ]
    }
  }
}
