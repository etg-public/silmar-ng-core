import { Pipe, PipeTransform } from '@angular/core';
import { MoneyXrService } from './money-xr.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Pipe({
  name : 'moneyXr'
})
export class MoneyXrPipe implements PipeTransform {
  constructor(protected xr: MoneyXrService) {

  }

  transform(amount: string | number, from: string, to: string, date: string = null, fixedFloat: number = 4): Observable<string> {
    return this.xr.convert(from, to, amount, date, fixedFloat)
      .pipe(
        map(fixer => fixer.amount)
      );
  }
}
