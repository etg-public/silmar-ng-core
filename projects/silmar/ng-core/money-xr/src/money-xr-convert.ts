export class MoneyXrConvert {
  base: string;

  rates: { [ currency: string ]: number };

  default = { from : 'EUR', to : 'EUR' };

  constructor(base: string = 'EUR', rates: { [ currency: string ]: number }) {
    this.base  = base;
    this.rates = Object.assign(rates, { [ this.base ] : 1 });
  }

  convert(val: string | number, from?: string, to?: string): number {
    // We need to know the `from` and `to` currencies
    from = from || this.default.from;
    to   = to || this.default.to;

    // Multiple the value by the exchange rate
    return val as number * this.getRate(to, from);
  }

  protected getRate(to, from): number {
    const rates = this.rates;

    // Throw an error if either rate isn't in the rates array
    if (!rates[ to ]) throw "Unknown currency given: " + to;
    if (!rates[ from ]) throw "Unknown currency given: " + from;

    // If `from` currency === fx.base, return the basic exchange rate for the `to` currency
    if (from === this.base) {
      return rates[ to ];
    }

    // If `to` currency === fx.base, return the basic inverse rate of the `from` currency
    if (to === this.base) {
      return 1 / rates[ from ];
    }

    // Otherwise, return the `to` rate multiplied by the inverse of the `from` rate to get the
    // relative exchange rate between the two currencies
    return rates[ to ] * (1 / rates[ from ]);
  }
}
