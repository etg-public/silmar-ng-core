export class ConversionData {
  base: string;
  to: string;
  date: string;
  amount: string;
}
