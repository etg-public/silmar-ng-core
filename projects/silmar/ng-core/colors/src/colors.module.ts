import { NgModule } from '@angular/core';
import { HashColorPipe } from './hash-color.pipe';

const COLORS = [
  HashColorPipe
];

@NgModule({
  imports      : [],
  declarations : COLORS,
  exports      : COLORS
})
export class ColorsModule {

}