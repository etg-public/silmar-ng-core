import { Pipe, PipeTransform } from '@angular/core';
import { Md5 } from 'ts-md5';
import { MATERIAL_PALLETE } from './common';

@Pipe({
  name: 'hashColor'
})
export class HashColorPipe implements PipeTransform {
  transform(value: string, pallete = '600'): string {
    if (!MATERIAL_PALLETE.hasOwnProperty(pallete)) {
      console.warn('Bad material pallete name. Choose one of: ', Object.keys(MATERIAL_PALLETE).join(', '));

      return '#ffffff';
    }

    return MATERIAL_PALLETE[ pallete ][ parseInt(Md5.hashStr(String(value)) as string, 16) % MATERIAL_PALLETE[ pallete ].length ];
  }
}
