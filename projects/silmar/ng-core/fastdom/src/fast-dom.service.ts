import { Injectable, NgZone } from '@angular/core';

/**
 * from FastDom
 *
 * https://github.com/wilsonpage/fastdom/blob/master/fastdom.js
 */

/**
 * Angular universal safeguard
 * */
const hasWin = typeof window != 'undefined';

/**
 * Normalized rAF
 */
const raf = hasWin
  && window
  && window.requestAnimationFrame
  || (cb => setTimeout(cb, hasWin ? 16 : 0));

export type TaskEntry = { n?: string, t: () => any };

@Injectable({
  providedIn : 'root'
})
export class FastDomService {
  /**
   * DOM measure queue
   */
  reads: TaskEntry[] = [];

  /**
   * DOM mutations queue
   */
  writes: TaskEntry[] = [];

  /**
   * rAF
   */
  raf = raf.bind(hasWin && window || this);

  /**
   * Flush scheduled flag
   */
  scheduled = false;

  constructor(protected zone: NgZone) {

  }

  /**
   * Adds a job to the read batch and
   * schedules a new frame if need be.
   *
   * The measure functions will always be executed AFTER the mutations
   * queue (if scheduled for the same frame) and right after the paint
   *
   * The given function will be called OUTSIDE Angular zone
   */
  measure(fn: () => any, ctx?: any, name?: string) {
    const task  = !ctx ? fn : fn.bind(ctx);
    const entry = { n : name ? 'r' + name : null, t : task } as TaskEntry;

    if (entry.n == null || this.reads.findIndex((item) => item.n == entry.n) < 0) {
      this.reads.push(entry);
      scheduleFlush(this, this.zone);
    }

    return task;
  }

  /**
   * Adds a job to the write batch and schedules
   * a new frame if need be.
   *
   * The given function will be called OUTSIDE Angular zone
   */
  mutate(fn: () => any, ctx?: any, name?: string) {
    const task  = !ctx ? fn : fn.bind(ctx);
    const entry = { n : name ? 'm' + name : null, t : task } as TaskEntry;

    if (entry.n == null || this.writes.findIndex((item) => item.n == entry.n) < 0) {
      this.writes.push(entry);
      scheduleFlush(this, this.zone);
    }

    scheduleFlush(this, this.zone);

    return task;
  }

  clear(task) {
    return remove(this.reads, task) || remove(this.writes, task);
  }
}

/**
 * Schedules a new read/write
 * batch if one isn't pending.
 */
function scheduleFlush(fastDom: FastDomService, zone: NgZone) {
  if (!fastDom.scheduled) {
    fastDom.scheduled = true;
    fastDom.raf(() => {
      zone.runOutsideAngular(() => {
        flush(fastDom, zone);
      });
    });
  }
}

/**
 * Runs queued `read` and `write` tasks.
 *
 * Errors are caught and thrown
 */
function flush(fastDom: FastDomService, zone: NgZone) {
  const writes = fastDom.writes;
  const reads  = fastDom.reads;
  let error;

  try {
    runTasks(writes);

    // delay the measures after the reflow, we need pristine DOM to make sure no reflow will be caused
    setTimeout(() => runTasks(reads));
  } catch (e) {
    error = e;
  }

  fastDom.scheduled = false;

  // If the batch errored we may still have tasks queued
  if (reads.length || writes.length) {
    scheduleFlush(fastDom, zone);
  }

  if (error) {
    throw error;
  }
}

/**
 * We run this inside a try catch
 * so that if any jobs error, we
 * are able to recover and continue
 * to flush the batch until it's empty.
 */
function runTasks(tasks: TaskEntry[]) {
  let entry;
  // tslint:disable-next-line:no-conditional-assignment
  while (entry = tasks.shift()) {
    entry.t();
  }
}

/**
 * Remove an item from an Array.
 */
function remove(array: TaskEntry[], item) {
  const index = array.findIndex((entry) => entry.t == item);

  index > -1 && array.splice(index, 1);

  return index;
}
