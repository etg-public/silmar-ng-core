import { Injectable, NgZone } from '@angular/core';
import { PingService } from './ping.service';

@Injectable({ providedIn : 'root' })
export class PingFactory {
  constructor(protected zone: NgZone) {}

  getPinger(interval: number = null, callback: any = null, args = [], context: any = null): PingService {
    return new PingService(this.zone, interval, callback, args, context);
  }
}
