import { EventEmitter, NgZone } from "@angular/core";
import { interval, Subject, Subscription } from "rxjs";
import { map, takeUntil } from "rxjs/operators";

/**
 * Ping service
 *
 * May be used for polling.
 */
export class PingService {
  /**
   * On ping event emitter
   */
  public onPing: EventEmitter<any> = new EventEmitter;

  /**
   * Ping interval in seconds
   */
  protected intervalTime: number = 5 * 60; // 5 min

  /**
   * Callback to be executed on ping
   */
  protected callback: any = () => {
  };

  /**
   * Callback arguments
   */
  protected args = [];

  /**
   * Callback context (eg. `this`)
   */
  protected context: any;

  protected subscription: Subscription;

  protected stopInterval$: Subject<boolean>;

  /**
   * Create new ping-er. All of the params can be omitted and configured later
   */
  constructor(protected zone: NgZone, interval: number = null, callback: any = null, args = [], context: any = null) {
    interval && (this.interval = interval);
    callback && (this.setCallback(callback, args, context));
  }

  /**
   * Get interval in seconds
   */
  get interval(): number {
    return this.intervalTime;
  }

  /**
   * Set interval in seconds
   */
  set interval(value: number) {
    if (this.intervalTime != value) {
      this.intervalTime = value;

      if (this.isRunning()) {
        this.start();
      }
    }
  }

  /**
   * Start the ping-er. If the ping-er is already running it will get REstarted
   */
  start(immediately: boolean = true): void {
    this.stop();

    // Start immediately
    immediately && setTimeout(_ => this.ping());

    this.stopInterval$ = new Subject<boolean>();
    this.subscription  = this.zone.runOutsideAngular(() =>
      interval(this.intervalTime * 1000)
        .pipe(
          takeUntil(this.stopInterval$),
          map(() => {
            this.zone.run(() => this.ping());
          })
        )
        .subscribe()
    );
  }

  /**
   * Stop the ping-er
   */
  stop(): void {
    if (this.stopInterval$) {
      this.stopInterval$.next(true);
      this.stopInterval$.complete();
    }
  }

  /**
   * Check if the ping-er is running
   */
  isRunning(): boolean {
    return !!(this.subscription && !this.subscription.closed);
  }

  /**
   * Set callback that will get executed on every ping
   */
  setCallback(callback: any, args = [], context: any = null): void {
    this.context  = context;
    this.callback = callback;
    this.args     = args;
  }

  /**
   * Execute a ping
   */
  ping(): void {
    this.onPing.emit(
      this.callback.apply(this.context || this, this.args)
    );
  }
}
