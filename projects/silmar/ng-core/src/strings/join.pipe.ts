import {Pipe, PipeTransform} from '@angular/core';

/**
 * Join pipe
 *
 * - `character` is the glue
 *
 * @example
 * <p>{{ ['Eiffel Tower', 'Paris', 'France'] | join:', '}}</p> -> 'Eiffel Tower, Paris, France'
 */
@Pipe({
    name: 'join'
})
export class JoinPipe implements PipeTransform {
    /**
     * @param input
     * @param character
     */
    transform(input: any, character: string = ''): any {

        if (!Array.isArray(input)) {
            return input;
        }

        return input.join(character);
    }
}
