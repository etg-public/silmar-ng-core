import { Pipe } from '@angular/core';
import { safeCompare } from '../common/numbers';

/**
 * Compare pipe
 *
 * Compares floats and prevents 45 - 44 (0.999999999999999999995) != 1 issues
 */
@Pipe({
  name : 'compare'
})
export class ComparePipe {
  transform(left: any, right: any, precision = 2): -1 | 0 | 1 {
    return safeCompare(left, right, precision);
  }
}