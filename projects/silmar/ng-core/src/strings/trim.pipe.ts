import {Pipe, PipeTransform} from '@angular/core';
import {isString, trim} from '../common/strings';

/**
 * Trim chars from string pipe
 *
 * - `chars` is a regex for chars to be removed (Default: \s (whitespace))
 *
 * @example
 * <span>{{'  John Doe ' | trim}}</span>         -> 'John Doe'
 * <span>{{'+359888123456' | trim:'+'}}</span> -> '359888123456'
 */
@Pipe({
    name: 'trim'
})
export class TrimPipe implements PipeTransform {
    /**
     * @param  text
     * @param  chars
     */
    transform(text: string, chars: string = null): string {
        return isString(text) ? trim(text, chars) : text;
    }
}
