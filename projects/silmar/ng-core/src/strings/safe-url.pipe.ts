import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name : 'safeUrl'
})
export class SafeUrlPipe implements PipeTransform {

  constructor(protected sanitizer: DomSanitizer) {}

  transform(url, resource: boolean = true) {
    return resource ?
      this.sanitizer.bypassSecurityTrustResourceUrl(url) :
      this.sanitizer.bypassSecurityTrustUrl(url);

  }
}
