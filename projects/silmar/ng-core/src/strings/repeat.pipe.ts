import {Pipe, PipeTransform} from '@angular/core';

/**
 * Repeat pipe
 *
 * - `count` is the number of reps
 *
 * @example
 * // let hotel = {stars: 3}
 *
 * <span>{{'★' | repeat:hotel.stars}}</span>
 *
 * // Result: '★★★'
 */
@Pipe({
    name: 'repeat'
})
export class RepeatPipe implements PipeTransform {
    /**
     * @param  value
     * @param  count
     */
    transform(value: string, count: number): any {
        let result = '';

        for (let i = 0; i < count; i++) {
            result += value;
        }

        return result;
    }
}
