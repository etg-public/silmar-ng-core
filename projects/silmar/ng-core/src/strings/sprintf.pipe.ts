import {Pipe, PipeTransform} from '@angular/core';

/**
 * Sprintf-like Pipe
 *
 * Will replace all `:key` occurrences with `key's` value from {key: "value"}
 *
 * - `args` is the key-value map
 *
 * @example
 * <span>{{"The quick :color fox jumps over the lazy :animal" | sprintf:{color:"brown", animal:"dog"} }}</span>
 *
 * // Result: 'The quick brown fox jumps over the lazy dog'
 */
@Pipe({
    name: 'sprintf'
})
export class SprintfPipe implements PipeTransform {
    /**
     * @param value
     * @param args
     */
    transform(value: any, args: any): any {
        if (typeof args == 'object' && value.indexOf(':') > -1) {
            let keys = Object.keys(args);

            for (let key of keys) {
                value = value.replace(new RegExp(':' + key, 'g'), args[key]);
            }
        }

        return value;
    }
}
