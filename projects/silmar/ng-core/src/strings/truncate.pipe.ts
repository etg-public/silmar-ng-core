import {Pipe} from '@angular/core';
import { truncate } from "../common/strings";

/**
 * Truncate pipe
 *
 * Shortens a long string
 *
 * - `limit` is the number of chars or words to be shown
 * - `trail` is a suffix to be added if string was truncated (Default: '...')
 * - `byWords` set to TRUE will use `limit` to count number of words before truncate
 *
 * @example
 * <span>{{'Chuck Norris counted to infinity... twice.' | truncate:15}}</span>
 * <span>{{'Chuck Norris counted to infinity... twice.' | truncate:3:'...':true}}</span>
 * <span>{{'Chuck Norris counted to infinity... twice.' | truncate:100}}</span>
 *
 * // Result:
 * // 'Chuck Norris co...'
 * // 'Chuck Norris counted...'
 * // 'Chuck Norris counted to infinity... twice.'
 */
@Pipe({
    name: 'truncate'
})
export class TruncatePipe {
    /**
     * @param value
     * @param limit
     * @param trail
     * @param byWords
     */
    transform(value: string, limit: number, trail: string = '...', byWords: boolean = false): string {
        return truncate(value, limit, trail, byWords);
    }
}
