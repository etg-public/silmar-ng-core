import {Pipe, PipeTransform} from '@angular/core';
import {escapeRegExp} from '../common/strings';

/**
 * Highlight pipe
 *
 * <p [innerHtml]="'Highlight within text pipe' | highlight:'within'"></p>
 */
@Pipe({
    name: 'highlight'
})
export class HighlightPipe implements PipeTransform {
    transform(text: string, phrase: string): any {
        if (!phrase || !text) {
            return text;
        }

        return text.replace(new RegExp('(' + escapeRegExp(phrase) + ')', 'gi'), '<mark class="highlighted">$1</mark>');
    }
}
