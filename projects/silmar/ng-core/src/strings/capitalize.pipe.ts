import {Pipe} from '@angular/core';

/**
 * Capitalize pipe
 *
 * - `words` will capitalize each word if set to TRUE
 *
 * @example
 * <span>{{'this is an angular pipe' | capitalize}}</span>              -> 'This is an angular pipe'
 * <span>{{'this is an angular pipe' | capitalize:true}}</span>         -> 'This Is An Angular Pipe'
 * <span>{{['this', 'is', 'an angular pipe'] | capitalize:true}}</span> -> ['This', 'Is', 'An Angular Pipe']
 */
@Pipe({
    name: 'capitalize'
})
export class CapitalizePipe {
    /**
     * @param value
     * @param words
     */
    transform(value: string | string[], words: boolean = false): string | string[] {
        let isString = false;

        if (value && typeof value == 'string') {
            value = [value];
            isString = true;
        }

        value && (value = (<string[]>value).map(val => val ? (
                words ? (val + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                        return $1.toUpperCase();
                    })
                    : val.charAt(0).toUpperCase() + val.substr(1)
            ) : ''
        ));

        return value ? (isString ? value[0] : value) : '';
    }
}