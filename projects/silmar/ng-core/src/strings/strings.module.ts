import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CapitalizePipe } from './capitalize.pipe';
import { HighlightPipe } from './highlight.pipe';
import { JoinPipe } from './join.pipe';
import { RepeatPipe } from './repeat.pipe';
import { RepeatDirective } from './repeat.directive';
import { SprintfPipe } from './sprintf.pipe';
import { TrimPipe } from './trim.pipe';
import { TruncatePipe } from './truncate.pipe';
import { SubstitutePipe } from './substitute.pipe';
import { ComparePipe } from './compare.pipe';
import { SafeUrlPipe } from './safe-url.pipe';

const STRINGS = [
  CapitalizePipe,
  HighlightPipe,
  JoinPipe,
  RepeatPipe,
  RepeatDirective,
  SprintfPipe,
  TrimPipe,
  TruncatePipe,
  SubstitutePipe,
  ComparePipe,
  SafeUrlPipe,
];

@NgModule({
  imports      : [
    CommonModule
  ],
  declarations : STRINGS,
  exports      : STRINGS
})
export class StringsModule {

}
