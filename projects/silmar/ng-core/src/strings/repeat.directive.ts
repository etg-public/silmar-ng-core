import { ViewContainerRef, TemplateRef, Directive, Input } from '@angular/core';

/**
 * Max repeat count. We don't want exploding browsers.
 *
 */
export const MAX_REPEAT = 1000;

/**
 * Simple repeat directive
 *
 * @example <i class="fa fa-star" *siRepeat="5"></i> (result: ★★★★★)
 */
@Directive({
  selector : '[siRepeat]'
})
export class RepeatDirective {
  constructor(private viewContainer: ViewContainerRef,
              private template: TemplateRef<any>) {
  }

  /**
   * Repeat count
   * @param value
   */
  @Input() set siRepeat(value: number) {
    value = +value;

    if (value < 0 || value > MAX_REPEAT) {
      this.render(0);

      return;
    }

    this.render(value);
  }

  /**
   * Clone current view X times
   */
  protected render(count: number): void {
    this.viewContainer.clear();

    if (count == 0) {
      return;
    }

    for (let i = 0; i < count; i++) {
      this.viewContainer.createEmbeddedView(this.template, { index : i });
    }
  }
}
