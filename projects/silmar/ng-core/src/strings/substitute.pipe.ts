import { Pipe } from "@angular/core";
import { substitute } from "../common/strings";

/**
 * Substitute pipe
 *
 * Interpolates a string with the given data
 *
 * - `data` key => value object with data to be replaced
 *
 * @example
 * <span>{{'Chuck Norris counted to {what}... {how_many}.' | substitute:{"what":"infinity", "how_many": "twice"}}}</span>
 *
 * // Result:
 * // 'Chuck Norris counted to infinity... twice.'
 */
@Pipe({
  name : 'substitute'
})
export class SubstitutePipe {
  /**
   * @param value
   * @param data
   */
  transform(value: string, data: { [ p: string ]: string }): string {
    return substitute(value, data);
  }
}