/**
 * Make a string's first character uppercase
 * @param subject
 */
export function ucfirst(subject: string) {
    return subject.charAt(0).toUpperCase() + subject.substr(1);
}

/**
 * Make every word's first character uppercase
 * @param subject
 */
export function ucwords(subject: string) {
    return (subject + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, c => c.toUpperCase());
}

/**
 * From camel to underscore case
 * @param subject
 */
export function toUnderscore(subject: string) {
    return subject.charAt(0).toLowerCase() + subject.substr(1).replace(/[A-Z]/g, l => '_' + l.toLowerCase());
}

/**
 * From underscore or dashed to camel case
 * @param subject
 */
export function toCamel(subject: string) {
    return subject.replace(/(-|_)[a-z]/g, (l, dash) => l.toUpperCase().replace(dash, ''));
}


/**
 * Trim chars from string
 * @param value
 * @param chars Whitespaces by default
 */
export function trim(value: string, chars: string = null): string {
    if (typeof value !== 'string') {
        return value;
    }
    if (chars == null) {
        return value.trim();
    }

    chars = '[' + escapeRegExp(chars) + ']';

    return value.replace(new RegExp('^' + chars + '+|' + chars + '+$', 'g'), '');
}

/**
 * Escape regex string
 * @param str
 */
export function escapeRegExp(str) {
    return String(str).replace(/[\\^$*+?.()|[\]{}]/g, '\\$&');
}

/**
 * Check if value is of type RegExp
 *
 * @param value
 */
export function isRegExp(value: any): boolean {
    return Object.prototype.toString.call(value) === '[object RegExp]';
}

/**
 * Check if value is of type string
 *
 * @param value
 */
export function isString(value: any): boolean {
    return typeof value === 'string';
}

/**
 * Get a simple random(ish) string
 */
export function getRandomString(length: number = 16) {
    const chars = 'abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890';
    let str = '';

    for (let x = 0; x < length; x++) {
        str += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return str;
}

/**
 * Substitute {key} in string with value from { key: value } params
 */
export function substitute(text: string, params: { [p: string]: string } = {}): string {
    // Early return. No data or placeholders found.
    if (!params || text.indexOf('{') < 0 || !Object.keys(params)) {
        return text;
    }

    return text.replace(/{(\w+)}/g, (m, v) => params.hasOwnProperty(v) ? params[v] : v);
}

/**
 * Shortens a long string
 *
 * - `limit` is the number of chars or words to be shown
 * - `trail` is a suffix to be added if string was truncated (Default: '...')
 * - `byWords` set to TRUE will use `limit` to count number of words before truncate
 */
export function truncate(value: string, limit: number, trail: string = '...', byWords: boolean = false) {
    if (!value || typeof value != 'string') {
        return value;
    }

    // Limit by chars count
    if (!byWords) {
        return value.length > limit ? value.substring(0, limit) + trail : value;
    }

    // Limit by words count
    let result = value;
    let words = value.split(/\s+/);

    if (words.length > limit) {
        result = words.slice(0, limit).join(' ') + trail;
    }

    return result;
}

/**
 * Build and encode a query string
 */
export function urlQueryString(data: { [ key: string ]: string }, prependAmp: boolean = false) {
    let result = [];

    for (let d in data) {
        if (typeof data[ d ] !== 'undefined') {
            result.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[ d ]));
        }
    }

    return (prependAmp && result.length ? '?' : '') + result.join('&');
}
