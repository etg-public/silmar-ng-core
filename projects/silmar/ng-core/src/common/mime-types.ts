/**
 * Mime types helper class
 */
export class MimeTypes {
  /**
   * Get extension from mime-type
   */
  static byType(type: string): string {
    if (type.indexOf('/') < 0) {
      return type;
    }

    type = type.toLowerCase();

    return Object.keys(MAPPING).find(key => MAPPING[ key ] == type) || null;
  }

  /**
   * Get mime-type from extension
   */
  static byExt(extension: string): string {
    if (extension.indexOf('/') < 0) {
      return extension;
    }

    extension = extension.toLowerCase();
    extension.indexOf('.') == 0 && (extension = extension.substr(1));

    return MAPPING.hasOwnProperty(extension) ? MAPPING[extension] : null;
  }
}

/**
 *
 */
const MAPPING = {
  'txt'      : 'text/plain',
  'html'     : 'text/html',
  'htm'      : 'text/html',
  'css'      : 'text/css',
  'js'       : 'text/javascript',
  'json'     : 'application/json',
  'api+json' : 'application/vnd.api+json',
  'xml'      : 'application/xml',
  'swf'      : 'application/x-shockwave-flash',
  'flv'      : 'video/x-flv',
  'csv'      : 'text/csv',

  // images
  'png'  : 'image/png',
  'jpg'  : 'image/jpeg',
  'jpeg' : 'image/jpeg',
  'jpe'  : 'image/jpeg',
  'gif'  : 'image/gif',
  'bmp'  : 'image/bmp',
  'ico'  : 'image/x-icon',
  'tif'  : 'image/tiff',
  'tiff' : 'image/tiff',
  'svg'  : 'image/svg+xml',
  'svgz' : 'image/svg+xml',

  // archives
  'zip' : 'application/zip',
  'rar' : 'application/x-rar-compressed',
  'exe' : 'application/x-msdownload',
  'msi' : 'application/x-msdownload',
  'cab' : 'application/vnd.ms-cab-compressed',

  // audio/video
  'mp3' : 'audio/mpeg',
  'mov' : 'video/quicktime',
  'qt'  : 'video/quicktime',

  // adobe
  'pdf' : 'application/pdf',
  'psd' : 'image/vnd.adobe.photoshop',
  'ps'  : 'application/postscript',
  'ai'  : 'application/postscript',
  'eps' : 'application/postscript',

  // ms office
  'doc' : 'application/msword',
  'rtf' : 'application/rtf',
  'xls' : 'application/vnd.ms-excel',
  'ppt' : 'application/vnd.ms-powerpoint',

  // open office
  'odt' : 'application/vnd.oasis.opendocument.text',
  'ods' : 'application/vnd.oasis.opendocument.spreadsheet',
};
