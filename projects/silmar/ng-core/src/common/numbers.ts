/**
 *
 * @param value
 */
export function toNumber(value: any): number {
  return +value || 0;
}

/**
 * Get a formatted number (e.g. 1 236,56) and return number (e.g. 1236.56)
 *
 * @param value
 */
export function makeNumber(value: any): number {
  return toNumber((value + '').replace(/\s/g, '').replace(',', '.'));
}

/**
 * Check if value is of type number
 *
 * @param value
 */
export function isNumber(value: any): boolean {
  return typeof value === 'number' && isFinite(value);
}

/**
 * Round a number with precision
 */
export function round(number: number, decimals: number = 0) {
  const tmp = Math.pow(10, decimals);

  return Math.round(number * tmp) / tmp;
}

/**
 * Normalize a number from one range to another
 *
 */
export function normalizeToRange(fromValue: number, from: number[], to: number[], fallOutside: boolean = false) {
  const res = (fromValue - from[ 0 ]) * (to[ 1 ] - to[ 0 ]) / (from[ 1 ] - from[ 0 ]) + to[ 0 ];

  return res < to[ 0 ] ? (fallOutside ? res : to[ 0 ]) : res;
}

/**
 * Safe compare
 * Precision is number of decimal digits to be compared
 *
 * precision = 2 | 1.560 == 1.559 | delta < 0.01
 * precision = 3 | 1.560 != 1.559 | delta >= 0.01
 */
export function safeCompare(left, right, precision = 2): -1 | 0 | 1 {
  let epsilon = 10 ** (-1 * precision),
      delta   = left - right;

  if (Math.abs(delta) < epsilon) {
    // Equal
    return 0;
  }  else if (left > right) {
    // Left is greater
    return 1;
  }

  // Right is greater
  return -1;
}