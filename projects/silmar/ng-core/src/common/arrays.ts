
/**
 * Extract array intersection
 */
export function arrayIntersect(a: any[], b: any[]) {
    if (b.length > a.length) {
        [b, a] = [a, b];
    }

    return a.filter(e => b.indexOf(e) > -1);
}

/**
 * Shuffle array elements
 *
 */
export function shuffle<T>(array: T[]): T[] {
    const oldArray = [...array];
    let newArray = [];

    while (oldArray.length) {
        const i = Math.floor(Math.random() * oldArray.length);
        newArray = newArray.concat(oldArray.splice(i, 1));
    }

    return newArray;
}

/**
 * Get random element
 */
export function randomElement(elements: string[]) {
    return elements[ Math.floor(Math.random() * elements.length) ];
}
