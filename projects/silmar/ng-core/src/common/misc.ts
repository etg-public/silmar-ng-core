import {Subscription} from 'rxjs';
import {isDate} from './dates';
import {isRegExp} from './strings';

/**
 * Check if value is of type Window
 *
 * @param obj
 */
export function isWindow(obj): boolean {
    return obj && obj.window === obj;
}

/**
 * Check if value is a Function
 *
 * @param value
 */
export function isFunction(value: any): boolean {
    return typeof value === 'function';
}

/**
 * Check if value is defined
 *
 * @param value
 */
export function isDefined(value: any): boolean {
    return typeof value !== 'undefined';
}

/**
 * Check if value is NULL
 *
 * @param value
 */
export function isNull(value: any): boolean {
    return value === null;
}

/**
 * Check if the given param is blank
 *
 * @param obj
 */
export function isBlank(obj: any): boolean {
    return obj == null;
}

/**
 *
 * @param value
 */
export function isPresent(value) {
    return value !== null && value !== undefined;
}

/**
 * Check if value is NULL or undefined
 *
 * @param value
 */
export function isNullOrUndefined(value: any): boolean {
    return isNull(value) || !isDefined(value);
}

/**
 * Check for iterability
 */
export function isIterable(value: any) {
    // checks for null and undefined
    if (value == null) {
        return false;
    }

    return typeof value[Symbol.iterator] === 'function';
}

/**
 * Check if value is an Object
 * @param value
 */
export function isObject(value: any): boolean {
    if (value === null) {
        return false;
    }
    return (!Array.isArray(value) && ((typeof value === 'function') || (typeof value === 'object')));
}

/**
 * Check if the given object is empty
 *
 * @param obj
 */
export function isObjectEmpty(obj: {}) {
    if (obj === null) {
        return true;
    }

    for (let x in obj) {
        if (obj.hasOwnProperty(x)) return false;
    }

    return true;
}

/**
 * Check if value is in range, you can compare dates and numbers
 *
 * @param value
 * @param min
 * @param max
 * @param inclusive
 */
export function inRange(value, min, max, inclusive: boolean = true) {
    return inclusive ? (value >= min && value <= max) : (value > min && value < max);
}

/**
 * Check if the given params are equal to each other
 *
 * @param o1
 * @param o2
 */
export function equals(o1: any, o2: any): boolean {
    if (o1 === o2) {
        return true;
    }

    if (o1 === null || o2 === null) {
        return false;
    }

    if (o1 !== o1 && o2 !== o2) {
        return true; // NaN === NaN
    }

    const t1 = typeof o1;
    const t2 = typeof o2;

    let length, key, keySet;

    if (t1 === t2 && t1 === 'object') {
        if (Array.isArray(o1)) {
            if (!Array.isArray(o2)) {
                return false;
            }

            if ((length = o1.length) === o2.length) {
                for (key = 0; key < length; key++) {
                    if (!equals(o1[key], o2[key])) {
                        return false;
                    }
                }

                return true;
            }
        } else if (isDate(o1)) {
            if (!isDate(o2)) {
                return false;
            }

            return equals(o1.getTime(), o2.getTime());
        } else if (isRegExp(o1)) {
            if (!isRegExp(o2)) {
                return false;
            }

            return o1.toString() === o2.toString();
        } else {
            if (isWindow(o1) || isWindow(o2) || Array.isArray(o2) || isDate(o2) || isRegExp(o2)) {
                return false;
            }

            keySet = Object.create(null);

            for (key in o1) {
                if (!o1.hasOwnProperty(key) || key.charAt(0) === '$' || isFunction(o1[key])) {
                    continue;
                }

                if (!equals(o1[key], o2[key])) {
                    return false;
                }

                keySet[key] = true;
            }

            for (key in o2) {
                if (o2.hasOwnProperty(key) && !(key in keySet) && key.charAt(0) !== '$' && isDefined(o2[key]) && !isFunction(
                        o2[key])) {
                    return false;
                }
            }

            return true;
        }
    }

    return false;
}

/**
 * Generate a non-secure fast hash of a value
 *
 * @param value
 */
export function fastHash(value: string): number {
    let hash = 0;

    if (value.length == 0) return hash;

    for (let i = 0; i < value.length; i++) {
        const char = value.charCodeAt(i);

        hash = ((hash << 5) - hash) + char;
        hash = hash & hash; // Convert to 32bit integer
    }

    return hash;
}

/**
 * Divide an integer to equal parts
 *
 * @example divideEquals(6,3) -> [2, 2, 2]
 * @example divideEquals(5,3) -> [2, 2, 1]
 * @example divideEquals(4,3) -> [2, 1, 1]
 *
 * @param base
 * @param pieces
 */
export function divideEquals(base: number, pieces: number = 2): number[] {
    base = Math.floor(base);

    let tmp, parts = [];

    while (pieces > 0) {
        tmp = (tmp % 2 == 0) ? Math.floor(base / pieces) : Math.ceil(base / pieces);
        base -= tmp;
        pieces--;
        parts.push(tmp);
    }

    return parts;
}

/**
 * Unsubscribe an object of subscriptions
 *
 * @param subscriptions
 */
export function massUnsubscribe(subscriptions: { [key: string]: Subscription }) {
    const keys = Object.keys(subscriptions),
        iMax = keys.length;
    let i = 0;

    for (; i < iMax; i++) {
        subscriptions[keys[i]] && subscriptions[keys[i]].unsubscribe();
    }
}