import { addDays, format, subDays } from 'date-fns';
import { isString } from './strings';

/**
 * Check if value is of type Date
 *
 */
export function isDate(value: any): boolean {
  return Object.prototype.toString.call(value) === '[object Date]';
}

/**
 * Create date object for this moment in UTC
 */
export function now(time: boolean = false) {
  let date = new Date();

  !time && date.setHours(0, 0, 0, 0);

  return utcDate(date);
}

/**
 * Set the same date in UTC timezone
 */
export function utcDate(date: Date) {
  return new Date(
    Date.UTC(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      date.getHours(),
      date.getMinutes(),
      date.getSeconds()
    )
  );
}

/**
 * Get an object with dates before, after and inside the second period
 */
export function periodDiff(first: string[] | Date[], second: string[] | Date[]): { before: string[], inside: string[], after: string[] } {
  let result       = { before : [], inside : [], after : [] };
  const secondLast = second.length - 1;

  for (let date of first) {
    if (date < second[ 0 ]) {
      result.before.push(date);
    } else if (date >= second[ 0 ] && date <= second[ secondLast ]) {
      result.inside.push(date);
    } else if (date > second[ secondLast ]) {
      result.after.push(date);
    }
  }

  return result;
}


/**
 * Convert Date object or date string notations to ISO 8601 string
 */
const DATE_STRINGS = {
  today     : () => format(now(), 'yyyy-MM-dd'),
  now       : () => format(now(true), 'yyyy-MM-dd'),
  yesterday : () => format(subDays(now(), 1), 'yyyy-MM-dd'),
  tomorrow  : () => format(addDays(now(), 1), 'yyyy-MM-dd')
};

/**
 * Convert date object or relative string to ISO 8601 string
 *
 * @see DATE_STRINGS
 */
export function toIsoDate(v: any): string {
  if (isDate(v)) {
    return format(v, 'yyyy-MM-dd');
  }

  if (isString(v) && DATE_STRINGS.hasOwnProperty(v)) {
    return DATE_STRINGS[ v ]();
  }

  return v;
}
