import { Pipe, PipeTransform } from '@angular/core';
import { shuffle } from '../common/arrays';

/**
 * Filter pipe
 */
@Pipe({
  name : 'shuffle'
})
export class ShufflePipe implements PipeTransform {
  transform(items: any[]): any {
    if (!items || !Array.isArray(items) || !items.length) {
      return items;
    }

    return shuffle(items);
  }
}
