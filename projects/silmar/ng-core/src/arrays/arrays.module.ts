import { NgModule } from '@angular/core';
import { ShufflePipe } from "./shuffle.pipe";
import { FilterPipe } from "./filter.pipe";

const ARRAYS = [ ShufflePipe, FilterPipe ];

@NgModule({
  imports      : [],
  declarations : ARRAYS,
  exports      : ARRAYS
})
export class ArraysModule {

}