import { Pipe, PipeTransform } from '@angular/core';

/**
 * Filter pipe
 */
@Pipe({
  name : 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], callback: (item: any) => boolean): any {
    if (!items || !callback) {
      return items;
    }

    return items.filter(item => callback(item));
  }
}
