import { Level, Notification } from './common';

const defStyle    = '';
const levelStyles = {
    debug: 'color:green;font-style:italic',
    info: 'color:blue' + defStyle,
    warning: 'color:orange;text-decoration:underline' + defStyle,
    error: 'color:red;font-weight:bold;text-transform:uppercase' + defStyle
};

/**
 * Logger
 *
 * @example Logger.info('Info message ', { context_property, another_value });
 * @example Logger.debug('Debug message', { object, another_object });
 * @example Logger.warning('Checking for unknown endpoint resource, make sure this is not an error! "', resource, '" - "', endpoint, '"');
 * @example Logger.error('Unknown type for proxy redirect: ' + type, { type, ref });
 */
export class Logger {
    /**
     *
     */
    level: number;

    constructor(public name: string, level: number = Level.info) {
        this.level = level;
    }

    /**
     * Receive notifications
     *
     * @param notif
     */
    onServiceNotification(notif: Notification) {
        if (notif.type === 'LEVEL') {
            this.level = notif.payload.level;
        }
    };

    /**
     * Debug log
     * @param args
     */
    debug(...args) {
        this.log('debug', Level.debug, args);
    }

    /**
     * Info log
     * @param args
     */
    info(...args) {
        this.log('info', Level.info, args);
    }

    /**
     * Warning log
     * @param args
     */
    warning(...args) {
        this.log('warn', Level.warning, args, 'warning');
    }

    /**
     * Erro log
     * @param args
     */
    error(...args) {
        this.log('error', Level.error, args);
    }

    /**
     *
     * @param type
     * @param level
     * @param args
     * @param name
     */
    protected log(type: string, level: number, args: any[], name?: string) {
        if (this.level > level) {
            return;
        }

        if (args[0] && Array.isArray(args[0])) {
            this.handleTable(args[0], args[1]);
        } else {
            let t = this.addName(name || (type.charAt(0).toUpperCase() + type.substr(1)));

            for (let item of args) {
                if (typeof item == 'object') {
                    try {
                        item = JSON.stringify(item);
                    } catch {
                        item = '[Circular Object]';
                    }
                }

                t.push(item);
            }

            console[type].apply(console, t);
        }
    }

    /**
     * Handle table log
     *
     * @param table
     * @param columns
     */
    protected handleTable(table: {}[], columns?: string[]) {
        console.table(table, columns);
    }

    /**
     * Add style and name to the log line
     *
     * @param level
     * @param style
     */
    protected addName(level: string, style = ''): string[] {
        if (levelStyles.hasOwnProperty(level.toLowerCase())) {
            style += ';' + levelStyles[level.toLowerCase()];
        }

        return ['%c%s | %c' + level, 'color:blue' + defStyle, this.name, style];
    }

}
