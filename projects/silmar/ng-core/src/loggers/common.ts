/**
 * Notification interface
 */
export interface Notification {
  type: string;

  payload: {
    level?: number;
  };
}

/**
 * Levels
 *
 */
export const Level = {
  debug: 1,
  info: 2,
  warning: 3,
  error: 4,
  mute: 5
};

