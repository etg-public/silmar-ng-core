import { Injectable, OnDestroy, Optional } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { Logger } from './logger';
import { Level } from './common';
import { massUnsubscribe } from '../common/misc';

/**
 *
 */
export class LogOptions {
  level: number;
}

/**
 *
 */
const DEFAULT_OPTIONS: LogOptions = {
  level : Level.warning
};

/**
 *
 */
@Injectable({
  providedIn : 'root'
})
export class LogService implements OnDestroy {

  /**
   *
   */
  loggers: Map<string, Logger> = new Map<string, Logger>();

  /**
   *
   */
  protected serviceNotification: Subject<any> = new Subject<any>();

  /**
   *
   */
  protected defaultLevel: number;

  /**
   *
   */
  protected subscr: { [ key: string ]: Subscription } = {};

  /**
   *
   * @param options
   */
  constructor(@Optional() options?: LogOptions) {
    let { level }     = Object.assign({}, DEFAULT_OPTIONS, options);
    this.defaultLevel = level;

    this.openLogger('main', level);
  }

  /**
   *
   */
  ngOnDestroy() {
    massUnsubscribe(this.subscr);
  }

  /**
   *
   * @param level
   */
  set level(level: string | number) {
    if (typeof level == 'string') {
      level = Level[ level ];
    }

    this.serviceNotification.next({ type : 'LEVEL', payload : { level } });
    this.defaultLevel = <number>level;
  }

  /**
   *
   * @param  loggerName
   * @param  level
   */
  openLogger(loggerName: string, level: number = null): Logger {
    let existingLogger = this.loggers.get(loggerName);

    if (!existingLogger) {
      existingLogger = new Logger(loggerName, level || this.defaultLevel);

      this.loggers.set(loggerName, existingLogger);
      this.subscr[ loggerName ] = this.serviceNotification.subscribe(notif => existingLogger.onServiceNotification(notif));
    }

    return existingLogger;
  }

  /**
   *
   * @param loggerName
   */
  closeLogger(loggerName: string) {
    let existingLogger = this.loggers.get(loggerName);

    if (existingLogger) {
      this.loggers.delete(loggerName);
      this.subscr[ loggerName ] && this.subscr[ loggerName ].unsubscribe();
    }

    return existingLogger;
  }

  /**
   * Redirect log to a given logger.
   *
   * If the asked logger does not yet exists it will be created
   *
   * @param  loggerName
   */
  to(loggerName: string): Logger {
    if (this.loggers.has(loggerName)) {
      return this.loggers.get(loggerName);
    } else {
      return this.openLogger(loggerName);
    }
  }

  /**
   *
   * @param args
   */
  debug(...args) {
    this.to('main').debug(...args);
  }

  /**
   *
   * @param args
   */
  info(...args) {
    this.to('main').info(...args);
  }

  /**
   *
   * @param args
   */
  warning(...args) {
    this.to('main').warning(...args);
  }

  /**
   *
   * @param args
   */
  error(...args) {
    this.to('main').error(...args);
  }
}
