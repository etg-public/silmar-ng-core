import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ClickOnceDirective,
  ClickOutsideDirective,
  PreventDefaultDirective,
  SelfDirective,
  StopPropagationDirective
} from './event-handlers';

@NgModule({
  imports      : [ CommonModule ],
  exports      : [ StopPropagationDirective,
    PreventDefaultDirective,
    SelfDirective,
    ClickOnceDirective,
    ClickOutsideDirective ],
  declarations : [ StopPropagationDirective,
    PreventDefaultDirective,
    SelfDirective,
    ClickOnceDirective,
    ClickOutsideDirective ]
})
export class EventModifiersModule {

}
