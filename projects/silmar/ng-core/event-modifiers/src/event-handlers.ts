import { Directive, ElementRef, EventEmitter, OnDestroy, OnInit, Output, Renderer2 } from '@angular/core';
import { callOnce, ModifierBase } from './common';

/**
 * @credit goes to https://github.com/NetanelBasal/ngx-event-modifiers/blob/master/src/event-modifiers.directives.ts
 *
 */

/**
 * The click event's propagation will be stopped
 * Usage:
 * <button (click.stop)="onClick($event, extraData)" [prevent]="true">Click Me!!</button>
 */
@Directive({
  selector : '[click.stop]'
})
export class StopPropagationDirective extends ModifierBase implements OnInit, OnDestroy {
  @Output('click.stop') stopPropEvent = new EventEmitter<MouseEvent>();

  stop = true;

  constructor(private rend: Renderer2, private el: ElementRef) {
    super();
  }

  ngOnInit() {
    this.unsubscribe = this.rend.listen(this.el.nativeElement, 'click', event => {
      this.doHandleEvents(event).stopPropEvent.emit(event);
    });
  }
}

/**
 * The submit event will no longer reload the page
 * Usage:
 * <button (click.prevent)="onClick($event, extraData)" [stop]="true">Click Me!!</button>
 */
@Directive({
  selector : '[click.prevent]'
})
export class PreventDefaultDirective extends ModifierBase implements OnInit, OnDestroy {
  @Output('click.prevent') preventDefaultEvent = new EventEmitter<MouseEvent>();

  prevent = true;

  constructor(private rend: Renderer2, private el: ElementRef) {
    super();
  }

  ngOnInit() {
    this.unsubscribe = this.rend.listen(this.el.nativeElement, 'click', event => {
      this.doHandleEvents(event).preventDefaultEvent.emit(event);
    });
  }
}

/**
 *
 * Only trigger handler if event.target is the element itself i.e. not from a child element
 * Usage:
 * <div (click.self)="onClick($event, extraData)">
 *   <button>Click Me!!</button>
 * </div>
 *  <div (click.self)="onClick($event, extraData)" [prevent]="true" [stop]="true">
 *   <button>Click Me!!</button>
 * </div>
 *
 */
@Directive({
  selector : '[click.self]'
})
export class SelfDirective extends ModifierBase implements OnInit, OnDestroy {
  @Output('click.self') selfEvent = new EventEmitter<MouseEvent>();

  constructor(private rend: Renderer2, private el: ElementRef) {
    super();
  }

  ngOnInit() {
    this.unsubscribe = this.rend.listen(this.el.nativeElement, 'click', event => {
      if (event.target === this.el.nativeElement) {
        this.doHandleEvents(event);
        this.selfEvent.emit(event);
      }
    });
  }
}

/**
 * The click event will be triggered at most once
 * Usage:
 * <button (click.once)="onClick($event, extraData)" [prevent]="true">Click Me!!</button>
 */
@Directive({
  selector : '[click.once]'
})
export class ClickOnceDirective extends ModifierBase implements OnInit, OnDestroy {
  @Output('click.once') onceEvent = new EventEmitter<MouseEvent>();

  constructor(private rend: Renderer2, private el: ElementRef) {
    super();
  }

  ngOnInit() {
    const eventFn    = callOnce(event => {
      this.doHandleEvents(event).onceEvent.emit(event);
    });
    this.unsubscribe = this.rend.listen(this.el.nativeElement, 'click', eventFn);
  }

}

/**
 * The click event will be triggered only if clicked outside the current element
 * Usage:
 * <button (click.outside)="onClick($event, extraData)">Click Me!!</button>
 */
@Directive({
  selector : '[click.outside]'
})
export class ClickOutsideDirective extends ModifierBase implements OnInit, OnDestroy {
  @Output('click.outside') clickOutsideEvent = new EventEmitter<MouseEvent>();

  constructor(private rend: Renderer2, private el: ElementRef) {
    super();
  }

  ngOnInit() {
    this.unsubscribe = this.rend.listen('document', 'click', event => {
      this.doHandleEvents(event);

      if (!this.el.nativeElement.contains(event.target)) {
        this.clickOutsideEvent.emit(event);
      }
    });
  }
}
