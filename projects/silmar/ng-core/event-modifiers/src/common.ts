import { Input, OnDestroy, Directive } from '@angular/core';

/**
 *
 * @param func
 */
export function callOnce(func: (event: any) => void) {
  let memo;

  return (event) => {
    if (func === null) {
      return memo;
    }

    memo = func.apply(this, [ event ]);
    func = null;

    return memo;
  };
}

/**
 *
 */
@Directive()
export class ModifierBase implements OnDestroy {
  /**
   *
   */
  @Input() prevent: boolean;

  /**
   *
   */
  @Input() stop: boolean;

  /**
   *
   */
  protected unsubscribe: () => void;

  ngOnDestroy() {
    this.unsubscribe && this.unsubscribe();
  }

  protected doHandleEvents(event) {
    this.prevent && event.preventDefault();
    this.stop && event.stopPropagation();

    return this;
  }
}
