import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Observable } from 'rxjs';
import { Permission, PushNotification } from './push-notification';

declare const Notification: any;

@Injectable({ providedIn : 'root' })
export class PushNotificationsService {

  permission: Permission;

  /**
   *
   */
  protected isBrowser: boolean;

  constructor(@Inject(PLATFORM_ID) platformId) {
    this.isBrowser  = isPlatformBrowser(platformId);
    this.permission = this.isSupported() ? Notification.permission : 'denied';
  }

  isSupported() {
    return !!(this.isBrowser && ('Notification' in window));
  }

  requestPermission() {
    if (this.isSupported()) {
      Notification.requestPermission((status: any) => this.permission = status);
    }
  }

  create(title: string, options?: PushNotification): any {
    if (!this.isBrowser) {
      return;
    }

    return new Observable((obs: any) => {
      if (this.permission !== 'granted') {
        obs.error(`The user hasn't granted you permission to send push notifications`);
        obs.complete();
      }

      const n = new Notification(title, options);

      n.onshow  = (e: any) => obs.next({ notification : n, event : e });
      n.onclick = (e: any) => obs.next({ notification : n, event : e });
      n.onerror = (e: any) => obs.error({ notification : n, event : e });
      n.onclose = () => obs.complete();
    });
  }
}
