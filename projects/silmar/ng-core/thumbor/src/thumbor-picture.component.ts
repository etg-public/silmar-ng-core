import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, Directive,
  ElementRef,
  Inject,
  Input, OnDestroy,
  OnInit, QueryList, Renderer2,
  ViewChild, ViewChildren
} from '@angular/core';
import { toNumber, isNumber } from '@silmar/ng-core';
import { ThumborUrl } from './thumbor-util';
import { THUMBOR_CONFIG, ThumborConfig } from './config';
import { ThumborAbstract } from './thumbor.abstract';
import { Subscription } from "rxjs";

@Directive({
  selector : '[siThumborSource]', exportAs : 'siThumborSource'
})
export class ThumborPictureSource {
  constructor(protected cd: ChangeDetectorRef, protected renderer: Renderer2, protected el: ElementRef) {}

  @Input() srcset(srcSet: string, attribute: string = 'srcset') {
    this.renderer.setAttribute(this.el.nativeElement, attribute, srcSet);
    this.cd.markForCheck();
  }
}

@Component({
  selector        : 'si-thumbor-picture',
  template        : `
    <picture>
      <ng-container *ngFor="let image of images">
        <source siThumborSource [attr.media]="image.media">
      </ng-container>
      <img [attr.alt]="alt" [attr.title]="title" #img/>
    </picture>
  `,
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class ThumborPicture extends ThumborAbstract implements OnInit, AfterViewInit, OnDestroy {
  @ViewChildren(ThumborPictureSource) sources: QueryList<ThumborPictureSource>;

  @ViewChild('img', { static: true }) fallBackImg: ElementRef;

  /**
   * Path of the original image
   */
  @Input() path: string;

  /**
   * Alt attribute
   */
  @Input() alt: string = '';

  /**
   * Title attribute
   */
  @Input() title: string = '';

  /**
   * Images src array
   */
  images: { src: string[]; media: string }[] = [];

  /**
   * Fallback
   */
  fallBackSrc: string[] = [];

  /**
   * Map of breakpoints
   */
  protected map: {} = {
    'phone'        : 'max-width: 599px',
    'phone-max'    : 'max-width: 899px',
    'tablet-p'     : '(min-width: 600px) and (max-width: 899px)',
    'tablet-p-min' : 'min-width: 600px',
    'tablet-l'     : '(min-width: 900px) and (max-width: 1279px)',
    'tablet-l-min' : 'min-width: 900px',
    'tablet-max'   : 'max-width: 1279px',
    'hd'           : '(min-width: 1280px) and (max-width: 1919px)',
    'desktop-min'  : 'min-width: 1280px',
    'fullhd'       : 'min-width: 1920px',
  };

  protected _srcSet: { [ key: string ]: string };

  protected init: boolean = false;

  protected sourceChanges$: Subscription;

  constructor(@Inject(THUMBOR_CONFIG) protected config: ThumborConfig, protected cd: ChangeDetectorRef, protected renderer: Renderer2) {
    super();
  }

  /**
   * Pass object of media query => size
   *
   * @example
   * {
     *      'min-width: 750px' : 150,
     *      'min-width: 990px' : '150x200',
     *      'hd' : 300
     * }
   */
  @Input() set bySize(srcSet: { [ key: string ]: string }) {
    this._srcSet = srcSet;

    this.init && this.buildSources();
  }

  ngAfterViewInit() {
    this.setSources();

    this.sourceChanges$ = this.sources.changes.subscribe((a) => {
      this.setSources();
    });
  }

  ngOnDestroy() {
    this.sourceChanges$ && this.sourceChanges$.unsubscribe();
  }

  protected buildSources() {
    const mediaTypes = Object.keys(this._srcSet);
    this.images      = [];
    this.fallBackSrc = [];
    let thumbor      = new ThumborUrl(this.config.thumborEndpoint, this.config.imagesDomain);

    for (let media of mediaTypes) {
      let isSize = !isNumber(this._srcSet[ media ]) && this._srcSet[ media ].indexOf('x') > -1,
          width  = toNumber(isSize ? this._srcSet[ media ].split('x')[ 0 ] : this._srcSet[ media ]),
          height = toNumber(isSize ? this._srcSet[ media ].split('x')[ 1 ] : this._srcSet[ media ]),

          src    = [
            thumbor
              .trim(this.trim)
              .fitIn(this.fitIn)
              .flipX(this.flipX)
              .flipY(this.flipY)
              .width(width)
              .height(height)
              .smart(this.smart)
              .filters(this.filters)
              .asWebp(this.config.supportsWebp && this.autoWebp)
              .getSrc(this.path)
          ];

      if (this.x2) {
        src.push(thumbor.width(width * 2).height(height * 2).getSrc(this.path) + ' 2x');
      }

      media = (this.map[ media ] ? this.map[ media ] : media);
      this.images.push({ src, media : media.indexOf('(') > -1 ? media : '(' + media + ')' });
    }

    this.fallBackSrc.push(thumbor
      .trim(this.trim)
      .fitIn(this.fitIn)
      .flipX(this.flipX)
      .flipY(this.flipY)
      .width(this.width)
      .height(this.height)
      .smart(this.smart)
      .filters(this.filters)
      .asWebp(this.config.supportsWebp && this.autoWebp)
      .getSrc(this.path));

    if (this.x2) {
      this.fallBackSrc.push(thumbor.width(this.width * 2).height(this.height * 2).getSrc(this.path) + ' 2x');
    }

    this.renderer.setAttribute(this.fallBackImg.nativeElement, this.mapping.srcset, this.fallBackSrc.join(', '));
    this.renderer.setAttribute(this.fallBackImg.nativeElement, this.mapping.src, this.fallBackSrc[ 0 ]);

    // set width and height of the image if available
    this.width && this.renderer.setAttribute(this.fallBackImg.nativeElement, 'width', '' + this.width);
    this.height && this.renderer.setAttribute(this.fallBackImg.nativeElement, 'height', '' + this.height);

    this.cd.markForCheck();
  }

  protected setSources() {
    let els = this.sources.toArray();

    for (let i in els) {
      els[ i ].srcset(this.images[ i ].src.join(', '), this.mapping.srcset);
    }

    this.cd.markForCheck();
  }

  protected render() {
    this.buildSources();
  }
}