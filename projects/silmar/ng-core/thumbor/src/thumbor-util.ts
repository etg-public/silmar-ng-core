export type TrimType = true | false | 'top-left' | 'bottom-right';

export type FitInType = true | false | 'fit-in' | 'full-fit-in' | 'adaptive-fit-in' | 'adaptive-full-fit-in';

export class ThumborUrl {
  protected _trim: TrimType = false;

  protected _fitIn: FitInType = false;

  protected _flipX: boolean = false;

  protected _flipY: boolean = false;

  protected _width: number = 0;

  protected _height: number = 0;

  protected _smart: boolean = false;

  protected _filters: { [ name: string ]: string[] } = {};

  protected hash = 'unsafe';

  constructor(public thumborEndpoint: string, public imagesDomain: string) {

  }

  getSrc(path: string, thumborEndpoint?: string, imagesDomain?: string): string {
    thumborEndpoint = thumborEndpoint ? thumborEndpoint : this.thumborEndpoint;
    imagesDomain    = imagesDomain ? imagesDomain : this.imagesDomain;

    return [
      thumborEndpoint,
      this.hash,
      this.trimmer,
      this.fitter,
      this.size,
      this.smartcrop,
      this.filter,
      imagesDomain,
      path
    ]
      .filter(v => v)
      .join('/');
  }

  asWebp(webp: boolean = false): this {
    if (webp) {
      this._filters[ 'format' ] = [ 'webp' ];
    }

    return this;
  }

  trim(trim: TrimType = false): this {
    this._trim = trim;
    return this;
  }

  fitIn(fitIn: FitInType = false): this {
    this._fitIn = fitIn;
    return this;
  }

  flipX(flipX: boolean = false): this {
    this._flipX = flipX;
    return this;
  }

  flipY(flipY: boolean = false): this {
    this._flipY = flipY;
    return this;
  }

  width(width: number = 0): this {
    this._width = width;
    return this;
  }

  height(height: number = 0): this {
    this._height = height;
    return this;
  }

  smart(smart: boolean = false): this {
    this._smart = smart;
    return this;
  }

  filters(filters: { [ name: string ]: string[] }): this {
    this._filters = filters;
    return this;
  }

  get trimmer() {
    return this._trim === true ? 'trim' : (this._trim === false ? null : this._trim);
  }

  get fitter() {
    return this._fitIn === true ? 'fit-in' : (this._fitIn === false ? null : this._fitIn);
  }

  get size() {
    return (this._flipX ? '-' : '') + this._width + 'x' + (this._flipY ? '-' : '') + this._height;
  }

  get smartcrop() {
    return this._smart ? 'smart' : null;
  }

  get filter() {
    let filters = [ 'filters' ];

    for (let f of Object.keys(this._filters)) {
      filters.push(f + `(${this._filters[ f ].join('%2C')})`);
    }

    // No filters added
    if (filters.length === 1) {
      return null;
    }

    return filters.join(':');
  }
}