import { Directive, ElementRef, Inject, Input, OnInit, Renderer2 } from '@angular/core';
import { THUMBOR_CONFIG, ThumborConfig } from './config';
import { ThumborUrl } from './thumbor-util';
import { ThumborAbstract } from './thumbor.abstract';

@Directive({ selector : '[siThumbor]', exportAs : 'siThumbor' })
export class ThumborDirective extends ThumborAbstract implements OnInit {
  protected px1Image = 'data:image/gif;base64,R0lGODlhAQABAPAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';

  protected hash = 'unsafe';

  constructor(@Inject(THUMBOR_CONFIG) protected config: ThumborConfig, protected renderer: Renderer2, protected el: ElementRef) {
    super();
  }

  @Input() set siThumbor(path: string) {
    this.setForLazy();

    if (path) {
      this.origPath = path;
    }
  }

  ngOnInit() {
    this.setForLazy();
    super.ngOnInit();
  }

  protected setForLazy() {
    if (this.isLazyImage() && this.set1pxOnLazy) {
      this.renderer.setAttribute(this.el.nativeElement, 'src', this.px1Image);
    }
  }

  protected render() {
    let thumbor = (new ThumborUrl(this.config.thumborEndpoint, this.config.imagesDomain))
          .trim(this.trim)
          .fitIn(this.fitIn)
          .flipX(this.flipX)
          .flipY(this.flipY)
          .width(this.width)
          .height(this.height)
          .smart(this.smart)
          .filters(this.filters)
          .asWebp(this.config.supportsWebp && this.autoWebp),
        src     = thumbor.getSrc(this.origPath);

    if (this.x2) {
      this.renderer.setAttribute(this.el.nativeElement, this.mapping.srcset, [
        src,
        thumbor.width(this.width * 2).height(this.height * 2).getSrc(this.origPath) + ' 2x'
      ].join(', '));
    }

    this.renderer.setAttribute(this.el.nativeElement, this.mapping.src, src);
  }
}
