import { InjectionToken } from '@angular/core';

export const THUMBOR_CONFIG = new InjectionToken<ThumborConfig>('silmar-thumbor-module THUMBOR_CONFIG');

export interface ThumborConfig {
  imagesDomain: string;
  thumborEndpoint: string;
  supportsWebp?: boolean;
}