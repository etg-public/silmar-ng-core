import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ThumborDirective} from './thumbor.directive';
import {THUMBOR_CONFIG, ThumborConfig} from './config';
import { ThumborPicture, ThumborPictureSource } from './thumbor-picture.component';
import { StringsModule } from '@silmar/ng-core';


@NgModule({
    imports     : [CommonModule, StringsModule],
    exports     : [ThumborDirective, ThumborPicture],
    declarations: [ThumborDirective, ThumborPicture, ThumborPictureSource],
})
export class ThumborModule {
    static forRoot(thumborConfig?: ThumborConfig): ModuleWithProviders<ThumborModule> {
        return {
            ngModule : ThumborModule,
            providers: [{provide: THUMBOR_CONFIG, useValue: thumborConfig}]
        };
    }
}
