import { FitInType, TrimType } from './thumbor-util';
import { Input, OnChanges, OnInit, SimpleChanges, Directive } from '@angular/core';

const DATA_SRC_MAP = {
  src    : 'data-src',
  srcset : 'data-srcset'
};
const SRC_MAP      = {
  src    : 'src',
  srcset : 'srcset'
};

@Directive()
export abstract class ThumborAbstract implements OnChanges, OnInit {
  /**
   * Map image attributes that have to be set
   */
  @Input() mapping: { src: string, srcset: string } = {
    src    : 'src',
    srcset : 'srcset'
  };

  @Input() trim: TrimType = false;

  @Input() fitIn: FitInType = false;

  @Input() flipX: boolean = false;

  @Input() flipY: boolean = false;

  @Input() width: number = 0;

  @Input() height: number = 0;

  @Input() smart: boolean = false;

  @Input() filters: { [ name: string ]: string[] } = {};

  @Input() x2: boolean = false;

  @Input() set1pxOnLazy: boolean = false;

  @Input() autoWebp: boolean = false;

  @Input() set useDataSrc(useDataSrc: boolean) {
    this.mapping = useDataSrc ? DATA_SRC_MAP : SRC_MAP;
  }

  protected origPath: string;

  protected init: boolean = false;

  ngOnInit() {
    this.init = true;
    this.render();
  }

  isLazyImage() {
    return this.mapping.src.substr(0, 5) == 'data-' || this.mapping.srcset.substr(0, 5) == 'data-';
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.init) {
      this.render();
    }
  }

  protected abstract render(): void;
}