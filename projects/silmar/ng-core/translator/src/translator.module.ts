import {NgModule} from '@angular/core';
import {TranslatorPipe} from './i18n.pipe';

@NgModule({
    exports: [TranslatorPipe],
    declarations: [TranslatorPipe],
})
export class TranslatorModule {
}
