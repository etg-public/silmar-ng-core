import { Injectable, Optional } from '@angular/core';
import { isString, toCamel, toUnderscore, ucfirst, ucwords, Logger, LogService } from '@silmar/ng-core';

/**
 * String transformations
 */
const TRANSFORMATIONS = {
  lower      : v => v.toLowerCase(),
  upper      : v => v.toUpperCase(),
  ucfirst    : v => ucfirst(v),
  ucwords    : v => ucwords(v),
  camel      : v => toCamel(v),
  underscore : v => toUnderscore(v),
};

/**
 * Dictionary
 */
export class Dictionary {
  [ key: string ]: { [ key: string ]: string } | string
}

/**
 * Translate service
 */
@Injectable()
export class TranslateService {
  /**
   * Language (iso2-letter)
   */
  protected _lang: string = 'en';

  /**
   * Logger
   */
  protected logger: Logger;

  /**
   * Set up loggers
   * @param dictionary
   * @param logSvc
   */
  constructor(protected dictionary: Dictionary, @Optional() logSvc: LogService) {
    this.logger = logSvc ? logSvc.to('translate') : null;
  }

  /**
   *
   */
  get lang(): string {
    return this._lang;
  }

  /**
   * @param value
   */
  set lang(value: string) {
    this._lang = value.toLowerCase();
  }

  /**
   * Set dictionary
   *
   * @param d
   */
  setDictionary(d: Dictionary) {
    this.dictionary = d;

    return this;
  }

  /**
   * Translate `key` in `lang` with `transform`
   * @param key
   * @param lang
   * @param transform
   */
  translate(key: string, lang?: string, transform?: string): string {
    key          = key + '';
    let result   = '',
        keyFound,
        lowerKey = key.toLowerCase();

    lang = lang.toLowerCase() || this._lang;

    if (!this.dictionary.hasOwnProperty(key)) {
      if (!this.dictionary.hasOwnProperty(lowerKey)) {
        this.logger && this.logger.debug(`Cannot find '${key}' in dictionary`);

        result   = key;
        keyFound = false;
      } else {
        key      = lowerKey;
        keyFound = true;
      }
    } else {
      keyFound = true;
    }

    if (keyFound) {
      if (isString(this.dictionary[ key ])) {
        // Simple key -> value (no languages)
        result = <string>this.dictionary[ key ];
      } else if (!this.dictionary[ key ].hasOwnProperty(lang)) {
        // Missing language
        this.logger && this.logger.debug(`No translation in '${lang}' language for '${key}' found`);

        result = key;
      } else {
        // Everything is OK
        result = this.dictionary[ key ][ lang ];
      }
    }

    return transform && TRANSFORMATIONS.hasOwnProperty(transform) ? TRANSFORMATIONS[ transform ](result) : result;
  }
}
