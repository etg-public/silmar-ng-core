import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from "./translate.service";

@Pipe({
  name : 'siI18n'
})
export class TranslatorPipe implements PipeTransform {
  constructor(protected translateSvc: TranslateService) {}

  transform(value: string, lang: string, transform?: string): any {
    return this.translateSvc.translate(value, lang, transform);
  }
}
