import { CropModeType, CropType, FocusType, ImageTransform } from './interfaces';

/**
 * Class helper that build URLs for imagekit
 */
export class ImageKitUrl {
  /**
   * currently configured transformations
   */
  protected _tr: ImageTransform[] = [ {} ];

  constructor(public endpoint: string, public identifier: string, trs?: ImageTransform[]) {
    this._tr = trs ? trs : [ {} ];
  }

  /**
   * Reset all transformations
   */
  reset(trs?: ImageTransform[]): this {
    this._tr = trs ? trs : [ {} ];

    return this;
  }

  /**
   * Get the src string
   */
  getSrc(path: string, endpoint?: string, identifier?: string): string {
    endpoint   = endpoint ? endpoint : this.endpoint;
    identifier = identifier ? identifier : this.identifier;

    const tr = this.trStrs(this._tr);

    return [
      endpoint,
      identifier,
      tr ? 'tr:' + tr : null,
      path
    ]
      .filter(v => v)
      .join('/');
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Add a new transformation sequence
   */
  addSequence(): this {
    this._tr[ this._tr.length ] = {};

    return this;
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Add single transformation to the last transformation sequence
   */
  addTr(tr: string, value: any): this {
    this._tr[ this._tr.length - 1 ][ tr ] = value;

    return this;
  }

  /**
   * Set DPR to the most recent transformation sequence
   */
  dpr(dpr: number | boolean) {
    (dpr > 1 || dpr === true) && (this._tr[ this._tr.length - 1 ].dpr = dpr === true ? 2 : dpr);

    return this;
  }

  /**
   * Add trim transformation (to the most recent transformation sequence)
   */
  trim(trim: number | false = false): this {
    trim !== false && (this._tr[ this._tr.length - 1 ].t = trim);

    return this;
  }

  /**
   * Set width in pixels (to the most recent transformation sequence)
   */
  width(width: number = 0): this {
    typeof width != 'undefined' && (this._tr[ this._tr.length - 1 ].w = width);

    return this;
  }

  /**
   * Set height in px (to the most recent transformation sequence)
   */
  height(height: number = 0): this {
    typeof height != 'undefined' && (this._tr[ this._tr.length - 1 ].h = height);

    return this;
  }

  /**
   * Add crop transformation (to the most recent transformation sequence)
   */
  crop(crop: CropType = 'maintain_ratio') {
    crop !== false && (this._tr[ this._tr.length - 1 ].c = crop === true ? 'maintain_ratio' : crop);

    return this;
  }

  /**
   * Change crop mode (to the most recent transformation sequence)
   */
  cmode(mode: CropModeType = 'resize') {
    mode !== false && (this._tr[ this._tr.length - 1 ].cm = mode === true ? 'resize' : mode);

    return this;
  }

  /**
   * Set focus mode (to the most recent transformation sequence)
   */
  focus(focus: FocusType = 'center') {
    focus !== false && (this._tr[ this._tr.length - 1 ].fo = focus === true ? 'center' : focus);

    return this;
  }

  /**
   * Shorthand for the focus mode auto or face (to the most recent transformation sequence)
   */
  smart(smart: boolean | 'face' = false): this {
    smart !== false && this.focus(smart == 'face' ? 'face' : 'auto');

    return this;
  }

  /**
   * Add named transformation (to the most recent transformation sequence)
   */
  named(name: string): this {
    name && (this._tr[ this._tr.length - 1 ].n = name);

    return this;
  }

  /**
   * Set background color, it accepts short form HEX color (to the most recent transformation sequence)
   */
  bgr(color: string) {
    color && (this._tr[ this._tr.length - 1 ].bg = color.length == 3 ? color + color : color);

    return this;
  }

  /**
   * Converts array of transformation objects to transformation string
   *
   * @see trStr
   */
  protected trStrs(trs: ImageTransform[]) {
    const r: string[] = [];

    for (const tr of trs) {
      r.push(this.trStr(tr));
    }

    return r.join(':');
  }

  // noinspection JSMethodCanBeStatic
  /**
   * Converts transformation object to transformation string
   */
  protected trStr(tr: ImageTransform) {
    const props       = Object.keys(tr);
    const r: string[] = [];

    for (const p of props) {
      const t = typeof tr[ p ];

      if (t != 'undefined') {
        r.push(p + '-' + (t == 'boolean' ? (tr[ p ] ? 'true' : 'false') : tr[ p ]));
      }
    }

    return r.join(',');
  }
}
