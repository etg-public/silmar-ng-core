
import { InjectionToken } from '@angular/core';


export const IMAGEKIT_CONFIG = new InjectionToken<ImageKitConfig>('silmar-imagekit IMAGEKIT_CONFIG');

export interface ImageKitConfig {
  identifier: string;
  endpoint: string;
  /**
   * no need
   */
  supportsWebp?: boolean;
}