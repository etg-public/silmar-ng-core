import { Input, OnChanges, OnInit, SimpleChanges, Directive } from '@angular/core';
import { CropModeType, CropType, FocusType, ImageTransform } from './interfaces';

const DATA_SRC_MAP = {
  src    : 'data-src',
  srcset : 'data-srcset'
};
const SRC_MAP      = {
  src    : 'src',
  srcset : 'srcset'
};

@Directive()
export abstract class ImagekitAbstract implements OnChanges, OnInit {
  /**
   * Map image attributes that have to be set
   */
  @Input() mapping: { src: string, srcset: string } = SRC_MAP;

  /**
   * Pass an array of custom transformations
   */
  @Input() transforms: ImageTransform[];

  /**
   * Use different identifier than the one configured
   */
  @Input() identifier: string;

  /**
   * Trim the image
   *
   * @see https://help.imagekit.io/articles/2434136-blur-radius-trim
   */
  @Input() trim: number | false = false;

  /**
   * Set width
   *
   * @see https://help.imagekit.io/articles/2512911-resizing-images-using-url-parameters
   */
  @Input() width: number;

  /**
   * Set height
   *
   * @see https://help.imagekit.io/articles/2512911-resizing-images-using-url-parameters
   */
  @Input() height: number;

  /**
   * Change background color
   *
   * @see https://help.imagekit.io/articles/2434120-background-and-border
   */
  @Input() bgr: string;

  /**
   * Set focus mode
   *
   * @see https://help.imagekit.io/articles/2503545-crop-crop-modes-and-focus
   */
  @Input() focus: FocusType = false;

  /**
   * Crop
   *
   * @see https://help.imagekit.io/articles/2503545-crop-crop-modes-and-focus
   */
  @Input() crop: CropType = false;

  /**
   * Crop mode
   *
   * @see https://help.imagekit.io/articles/2503545-crop-crop-modes-and-focus
   */
  @Input() cmode: CropModeType = false;

  /**
   * Shorthand for {@see crop} auto and face
   */
  @Input() smart: boolean | 'face' = false;

  /**
   * Change the DPR
   *
   * @see https://docs.imagekit.io/?ref=dashboard#commonly-used
   */
  @Input() x2: boolean | number;

  /**
   * Set named transformation
   *
   * @see https://help.imagekit.io/articles/2434121-what-are-named-transformations-and-their-advantages
   */
  @Input() named: string;

  /**
   * Set 1px transparent GIF if this is a lazy loaded image
   */
  @Input() set1pxOnLazy: boolean = false;

  /**
   * Instead of src, srcset use data-src and data-srcset
   */
  @Input() set useDataSrc(useDataSrc: boolean) {
    this.mapping = useDataSrc ? DATA_SRC_MAP : SRC_MAP;
  }

  protected origPath: string;

  protected init: boolean = false;

  ngOnInit() {
    this.init = true;
    this.render();
  }

  isLazyImage() {
    return this.mapping.src.substr(0, 5) == 'data-' || this.mapping.srcset.substr(0, 5) == 'data-';
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.init) {
      this.render();
    }
  }

  protected abstract render(): void;
}