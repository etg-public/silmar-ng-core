import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StringsModule } from '@silmar/ng-core';
import { IMAGEKIT_CONFIG, ImageKitConfig } from './config';
import { ImageKitDirective } from './imagekit.directive';
import { ImageKitPicture, ImageKitPictureSource } from './imagekit-picture.component';
import { ImagekitUrlPipe } from './imagekit-url.pipe';


@NgModule({
  imports      : [ CommonModule, StringsModule ],
  exports      : [ ImageKitDirective, ImageKitPicture, ImagekitUrlPipe ],
  declarations : [ ImageKitDirective, ImageKitPicture, ImageKitPictureSource, ImagekitUrlPipe ],
})
export class ImageKitModule {
  static forRoot(config: ImageKitConfig): ModuleWithProviders<ImageKitModule> {
    return {
      ngModule  : ImageKitModule,
      providers : [ { provide : IMAGEKIT_CONFIG, useValue : config } ]
    };
  }
}
