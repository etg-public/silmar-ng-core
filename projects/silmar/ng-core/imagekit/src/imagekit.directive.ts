import { Directive, ElementRef, Inject, Input, OnInit, Renderer2 } from '@angular/core';
import { IMAGEKIT_CONFIG, ImageKitConfig } from './config';
import { ImagekitAbstract } from './imagekit.abstract';
import { ImageKitUrl } from './imagekit-util';

@Directive({
  selector : '[siImageKit]',
  exportAs : 'siImageKit'
})
export class ImageKitDirective extends ImagekitAbstract implements OnInit {
  protected px1Img = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';

  constructor(@Inject(IMAGEKIT_CONFIG) protected config: ImageKitConfig, protected renderer: Renderer2, protected el: ElementRef) {
    super();
  }

  @Input() set siImageKit(path: string) {
    this.setForLazy();

    if (path) {
      this.origPath = path;
    }
  }

  ngOnInit() {
    this.setForLazy();
    super.ngOnInit();
  }

  protected setForLazy() {
    if (this.isLazyImage() && this.set1pxOnLazy) {
      this.renderer.setAttribute(this.el.nativeElement, 'src', this.px1Img);
    }
  }

  protected render() {
    let url = (new ImageKitUrl(this.config.endpoint, (this.identifier || this.config.identifier), this.transforms))
          .named(this.named)
          .width(this.width)
          .height(this.height)
          .trim(this.trim)
          .crop(this.crop)
          .cmode(this.cmode)
          .focus(this.focus)
          .smart(this.smart)
          .bgr(this.bgr),
        src = url.getSrc(this.origPath);

    if (this.x2) {
      this.renderer.setAttribute(this.el.nativeElement, this.mapping.srcset, [
        src,
        url.dpr(this.x2).getSrc(this.origPath) + ' 2x'
      ].join(', '));
    }

    this.renderer.setAttribute(this.el.nativeElement, this.mapping.src, src);
  }
}
