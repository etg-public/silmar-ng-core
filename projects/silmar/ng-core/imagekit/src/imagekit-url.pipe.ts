import { Inject, Pipe, PipeTransform } from '@angular/core';
import { ImageKitUrl } from './imagekit-util';
import { IMAGEKIT_CONFIG, ImageKitConfig } from './config';
import { ImageTransform } from './interfaces';

@Pipe({
  name : 'imagekitUrl'
})
export class ImagekitUrlPipe implements PipeTransform {
  constructor(@Inject(IMAGEKIT_CONFIG) protected config: ImageKitConfig) {

  }

  transform(src: string, width?: number, height?: number, trs?: ImageTransform[], identifier?: string): any {
    return (new ImageKitUrl(this.config.endpoint, this.config.identifier, trs))
      .width(width)
      .height(height)
      .getSrc(src, null, identifier);
  }
}
