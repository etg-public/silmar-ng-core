export type CropType = true | false | 'maintain_ratio' | 'force' | 'at_least' | 'at_max';

export type CropModeType = true | false | 'resize' | 'extract' | 'pad_extract' | 'pad_resize';

export type FocusType =
  true
  | false
  | 'center'
  | 'top'
  | 'left'
  | 'bottom'
  | 'right'
  | 'top_left'
  | 'top_right'
  | 'bottom_left'
  | 'bottom_right'
  | 'face'
  | 'auto';

export interface ImageTransform {
  /**
   * Width
   */
  w?: any,
  /**
   * Height
   */
  h?: any,
  /**
   *  aspect ratio (<width>-<height>)
   */
  ar?: string,
  /**
   * Crop
   */
  c?: CropType,
  /**
   * Crop mode
   */
  cm?: CropModeType,
  /**
   * Focus
   */
  fo?: FocusType,
  /**
   * quality
   */
  q?: any,
  /**
   * Format
   */
  f?: any,
  /**
   * blur
   */
  bl?: any,
  /**
   * device pixel ratio
   */
  dpr?: any,
  /**
   * Named transformations
   */
  n?: string,
  /**
   * default image
   */
  di?: string,
  /**
   * progressive JPEG
   */
  pr?: boolean,
  /**
   * Lossless WebP and PNG
   */
  lo?: boolean,
  /**
   * trim edges
   */
  t?: any,
  /**
   * radius
   */
  r?: number,
  /**
   * background color RGB or RGBA
   */
  bg?: string,
  /**
   * Border <width>_<color>
   */
  b?: string,
  /**
   * rotate (0, 90, 180, 270, 360 and auto)
   */
  rt?: number | string,
}