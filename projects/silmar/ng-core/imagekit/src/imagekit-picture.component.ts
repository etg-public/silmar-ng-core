import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Directive,
  ElementRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  Renderer2,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { isNumber, toNumber } from '@silmar/ng-core';
import { IMAGEKIT_CONFIG, ImageKitConfig } from './config';
import { Subscription } from "rxjs";
import { ImagekitAbstract } from './imagekit.abstract';
import { ImageKitUrl } from './imagekit-util';

@Directive({
  selector : '[siImageKitSource]', exportAs : 'siImageKitSource'
})
export class ImageKitPictureSource {
  constructor(protected cd: ChangeDetectorRef, protected renderer: Renderer2, protected el: ElementRef) {}

  @Input() srcset(srcSet: string, attribute: string = 'srcset') {
    this.renderer.setAttribute(this.el.nativeElement, attribute, srcSet);
    this.cd.markForCheck();
  }
}

@Component({
  selector        : 'si-imagekit-picture',
  template        : `
    <picture>
      <ng-container *ngFor="let image of images">
        <source siImageKitSource [attr.media]="image.media">
      </ng-container>
      <img [attr.alt]="alt" [attr.title]="title" #img/>
    </picture>
  `,
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class ImageKitPicture extends ImagekitAbstract implements OnInit, AfterViewInit, OnDestroy {
  @ViewChildren(ImageKitPictureSource) sources: QueryList<ImageKitPictureSource>;

  @ViewChild('img', { static : true }) fallBackImg: ElementRef;

  /**
   * Path of the original image
   */
  @Input() path: string;

  /**
   * Alt attribute
   */
  @Input() alt: string = '';

  /**
   * Title attribute
   */
  @Input() title: string = '';

  /**
   * Images src array
   */
  images: { src: string[]; media: string }[] = [];

  /**
   * Fallback
   */
  fallBackSrc: string[] = [];

  /**
   * Map of breakpoints
   */
  protected map: {} = {
    'phone'        : 'max-width: 599px',
    'phone-max'    : 'max-width: 899px',
    'tablet-p'     : '(min-width: 600px) and (max-width: 899px)',
    'tablet-p-min' : 'min-width: 600px',
    'tablet-l'     : '(min-width: 900px) and (max-width: 1279px)',
    'tablet-l-min' : 'min-width: 900px',
    'tablet-max'   : 'max-width: 1279px',
    'hd'           : '(min-width: 1280px) and (max-width: 1919px)',
    'desktop-min'  : 'min-width: 1280px',
    'fullhd'       : 'min-width: 1920px',
  };

  protected _srcSet: { [ key: string ]: string | number };

  protected init: boolean = false;

  protected sources$: Subscription;

  constructor(@Inject(IMAGEKIT_CONFIG) protected config: ImageKitConfig, protected cd: ChangeDetectorRef, protected renderer: Renderer2) {
    super();
  }

  /**
   * Pass object of media query => size
   *
   * @example
   * {
   *      'min-width: 750px' : 150,
   *      'min-width: 990px' : '150x200',
   *      'hd' : 300
   * }
   */
  @Input() set bySize(srcSet: { [ key: string ]: string | number }) {
    this._srcSet = srcSet;

    this.init && this.buildSources();
  }

  ngAfterViewInit() {
    this.setSources();

    this.sources$ = this.sources.changes.subscribe((a) => {
      this.setSources();
    });
  }

  ngOnDestroy() {
    this.sources$ && this.sources$.unsubscribe();
  }

  protected buildSources() {
    const mediaTypes = Object.keys(this._srcSet);
    const url        = new ImageKitUrl(this.config.endpoint, (this.identifier || this.config.identifier), this.transforms);
    this.images      = [];
    this.fallBackSrc = [];
    let defWidth     = this.width,
        defHeight    = this.height;

    for (let media of mediaTypes) {
      let srcset = (this._srcSet[ media ] as string),
          isSize = !isNumber(this._srcSet[ media ]) && srcset.indexOf('x') > -1,
          width  = toNumber(isSize ? srcset.split('x')[ 0 ] : this._srcSet[ media ]),
          height = toNumber(isSize ? srcset.split('x')[ 1 ] : this._srcSet[ media ]),

          src    = [
            url
              .reset(this.transforms)
              .named(this.named)
              .width(width)
              .height(height)
              .trim(this.trim)
              .crop(this.crop)
              .cmode(this.cmode)
              .focus(this.focus)
              .smart(this.smart)
              .bgr(this.bgr)
              .getSrc(this.path)
          ];

      if (this.x2) {
        src.push(url.dpr(this.x2).getSrc(this.path) + ' 2x');
      }

      media = (this.map[ media ] ? this.map[ media ] : media);
      this.images.push({ src, media : media.indexOf('(') > -1 ? media : '(' + media + ')' });

      if (!defWidth) {
        defWidth  = width;
        defHeight = height;
      }
    }

    this.fallBackSrc.push(
      url.reset(this.transforms)
        .named(this.named)
        .width(this.width || defWidth)
        .height(this.height || defHeight)
        .trim(this.trim)
        .crop(this.crop)
        .cmode(this.cmode)
        .focus(this.focus)
        .smart(this.smart)
        .bgr(this.bgr)
        .getSrc(this.path)
    );

    if (this.x2) {
      this.fallBackSrc.push(url.dpr(this.x2).getSrc(this.path) + ' 2x');
    }

    this.renderer.setAttribute(this.fallBackImg.nativeElement, this.mapping.srcset, this.fallBackSrc.join(', '));
    this.renderer.setAttribute(this.fallBackImg.nativeElement, this.mapping.src, this.fallBackSrc[ 0 ]);

    // set width and height of the image if available
    this.width && this.renderer.setAttribute(this.fallBackImg.nativeElement, 'width', '' + this.width);
    this.height && this.renderer.setAttribute(this.fallBackImg.nativeElement, 'height', '' + this.height);

    this.cd.markForCheck();
  }

  protected setSources() {
    let els = this.sources.toArray();

    for (let i in els) {
      els[ i ].srcset(this.images[ i ].src.join(', '), this.mapping.srcset);
    }

    this.cd.markForCheck();
  }

  protected render() {
    this.buildSources();
  }
}
