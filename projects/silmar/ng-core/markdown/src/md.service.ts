import { Injectable, Inject, Optional } from '@angular/core';
import { MARKDOWN_CONFIG, MarkdownConfig } from "./config";
import { marked } from 'marked';
import { isDefined, isNull, ucfirst, LogService } from "@silmar/ng-core";
import { Renderer } from "./plain-text-renderer";

@Injectable({
  providedIn : 'root',
})
export class MdService {
  md: any;

  constructor(@Inject(MARKDOWN_CONFIG) config: MarkdownConfig, @Optional() private logger: LogService) {
    // make sure we switch ON GFM in case somebody uses the deprecated (since 0.7.0) options `tables`
    if (config && config.tables) {
      config.gfm = true;
    }

    this.md = marked.setOptions(config);
  }

  parse(string: string, plainText: boolean = false, options?: MarkedOptions) {
    // noinspection SuspiciousTypeOfGuard
    if (typeof string != "string") {
      this.logger && this.logger.warning(
        'Parsing markdown from non string. Conversion from ' + ucfirst(typeof string) + ' was tried.'
      );

      (isNull(string) || !isDefined(string)) && (string = '');
      string = String(string);
    }

    if (plainText) {
      options = Object.assign({}, options, { renderer : new Renderer() });
    }

    return this.md.parse(string, options);
  }
}

export interface MarkedOptions {
  gfm?: boolean;
  /**
   * @deprecated
   */
  tables?: boolean;
  breaks?: boolean;
  pedantic?: boolean;
  /**
   * @deprecated
   */
  sanitize?: boolean;
  smartLists?: boolean;
  smartypants?: boolean;
}
