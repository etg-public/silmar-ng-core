import {InjectionToken} from '@angular/core';
import { Renderer } from "marked";

export const MARKDOWN_CONFIG = new InjectionToken<MarkdownConfig>('silmar-markdown-module MARKDOWN_CONFIG');

export interface MarkdownConfig {
  renderer?: Renderer,
  gfm?: boolean,
  /**
   * @deprecated
   */
  tables?: boolean,
  breaks?: boolean,
  pedantic?: boolean,
  /**
   * @deprecated
   */
  sanitize?: boolean,
  smartLists?: boolean,
  smartypants?: boolean
}