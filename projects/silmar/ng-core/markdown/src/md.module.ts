import { NgModule, ModuleWithProviders } from '@angular/core';
import { MdPipe } from "./md.pipe";
import { MARKDOWN_CONFIG, MarkdownConfig } from "./config";

@NgModule({
  declarations : [ MdPipe ],
  exports      : [ MdPipe ]
})
export class MdModule {
  static forRoot(markdownConfig?: MarkdownConfig): ModuleWithProviders<MdModule> {
    return {
      ngModule  : MdModule,
      providers : [
        { provide : MARKDOWN_CONFIG, useValue : markdownConfig }
      ]
    };
  }
}
