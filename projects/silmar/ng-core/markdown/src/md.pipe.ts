import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { MdService } from "./md.service";

@Pipe({
  name : 'md'
})
export class MdPipe implements PipeTransform {
  constructor(private mdSvc: MdService, protected sanitizer: DomSanitizer) {}

  transform(value: string, plainText: boolean = false, sanitize: boolean = true, safeHtml: boolean = true): string {
    const txt = this.mdSvc.parse(value, plainText);

    return safeHtml ? this.sanitizer.bypassSecurityTrustHtml(txt) : txt;
  }
}
