import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, NgModule, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector : 'si-fieldset',
  template : `
    <fieldset class="ui-fieldset ui-widget ui-widget-content"
              [ngClass]="{'ui-fieldset-toggleable': toggleable}"
              [ngStyle]="style" [class]="styleClass">
      <legend class="ui-fieldset-legend ui-corner-all ui-state-default ui-unselectable-text">
        <ng-container *ngIf="toggleable; else legendContent">
          <a href="#" (click)="toggle($event)" [attr.tabindex]="toggleable ? null : -1">
            <ng-container *ngTemplateOutlet="legendContent"></ng-container>
          </a>
        </ng-container>
        <ng-template #legendContent>
          <span class="ui-fieldset-toggler fal fa-fw" *ngIf="toggleable"
                [ngClass]="{'fa-minus': !collapsed,'fa-plus': collapsed}"></span>
          <span class="ui-fieldset-legend-text" *ngIf="legend">{{legend}}</span>
          <ng-content select="si-fieldset-header"></ng-content>
        </ng-template>
      </legend>
      <div class="ui-fieldset-content-wrapper" [ngClass]="{'ui-fieldset-content-wrapper-overflown': collapsed}">
        <div class="ui-fieldset-content" *ngIf="!collapsed">
          <ng-content></ng-content>
        </div>
      </div>
    </fieldset>
  `,
  styles   : [
      `:host, fieldset {
          display: flex;
      }

      fieldset {
          flex: 1;
      }
      
      .ui-fieldset-toggleable .ui-fieldset-legend a {
          color: inherit;
      }

      .ui-fieldset-toggler, .ui-chkbox-box, .ui-radiobutton-box {
          box-sizing: border-box;
      }

      .ui-fieldset .ui-fieldset-legend .ui-fieldset-toggler {
          width: 1.5em;
          height: 1.5em;
          font-size: 1em;
          padding: .25em;
          margin-right: .25em;
      }
    `
  ]
})
export class FieldsetComponent {
  @Input() legend: string;

  @Input() toggleable: boolean;

  @Input() collapsed: boolean = false;

  @Output() collapsedChange: EventEmitter<any> = new EventEmitter();

  @Output() onBeforeToggle: EventEmitter<any> = new EventEmitter();

  @Output() onAfterToggle: EventEmitter<any> = new EventEmitter();

  @Input() style: any;

  @Input() styleClass: string;

  constructor(protected el: ElementRef, protected cd: ChangeDetectorRef) {}

  toggle(event) {
    this.onBeforeToggle.emit({ originalEvent : event, collapsed : this.collapsed });

    if (this.collapsed) {
      this.expand(event);
    } else {
      this.collapse(event);
    }

    this.onAfterToggle.emit({ originalEvent : event, collapsed : this.collapsed });

    event.preventDefault();
  }

  expand(event) {
    this.collapsed = false;
    this.collapsedChange.emit(this.collapsed);
  }

  collapse(event) {
    this.collapsed = true;
    this.collapsedChange.emit(this.collapsed);
  }
}

@Component({
  selector : 'si-fieldset-header',
  template : '<ng-content></ng-content>'
})
export class FieldsetHeaderComponent {
}

@NgModule({
  imports      : [ CommonModule ],
  exports      : [ FieldsetComponent, FieldsetHeaderComponent ],
  declarations : [ FieldsetComponent, FieldsetHeaderComponent ]
})
export class FieldsetModule {

}
