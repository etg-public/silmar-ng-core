import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, NgModule, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { CommonModule } from '@angular/common';
import { CheckboxComponent, CheckboxModule } from '@silmar/ng-core/components/checkbox';

export const THREE_STATE_CHECKBOX_VALUE_ACCESSOR: any = {
  provide     : NG_VALUE_ACCESSOR,
  useExisting : forwardRef(() => ThreeStateCheckbox),
  multi       : true
};

@Component({
  selector        : 'si-three-state-checkbox',
  template        : `
    <div class="ui-chkbox ui-widget">
      <div class="ui-helper-hidden-accessible">
        <input #cb type="checkbox" name="{{name}}" [checked]="checked" (focus)="onFocus($event)"
               (blur)="onBlur($event)" [ngClass]="{'ui-state-focus':focused}"
               (keydown.space)="onClick($event,cb,false)">
      </div>
      <div class="ui-chkbox-box ui-widget ui-corner-all ui-state-default" (click)="onClick($event,cb,true)"
           (mouseover)="hover=true" (mouseout)="hover=false"
           [ngClass]="backgroundClass(hover, disabled, checked, focused)">
        <span class="ui-chkbox-icon ui-c" [ngClass]="iconClass(checked)"></span>
      </div>
    </div>
    <label class="ui-chkbox-label" (click)="onClick($event,cb,true)" *ngIf="label">{{label}}</label>
  `,
  styles          : [ '.ui-chkbox .ui-state-false {border-color: #9a274d; background-color: #C62828 !important;}' ],
  providers       : [ THREE_STATE_CHECKBOX_VALUE_ACCESSOR ],
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class ThreeStateCheckbox extends CheckboxComponent {

  @Input() name: string;

  @Input() disabled: boolean;

  @Input() label: string;

  @Output() onChange: EventEmitter<any> = new EventEmitter();

  checked: boolean | null = null;

  hover: boolean = false;

  constructor(protected cdr: ChangeDetectorRef) {
    super(cdr);
  }

  onClick(event: any, checkbox: any, focus: boolean): void {
    event.preventDefault();

    if (this.disabled) {
      return;
    }

    // Checked states: NULL > TRUE > FALSE
    switch (this.checked) {
      case true :
        this.checked = false;
        break;
      case false :
        this.checked = null;
        break;
      case null :
        this.checked = true;
        break;
      default :
        break;
    }

    this.onModelChange(this.checked);

    this.onChange.emit(this.checked);

    if (focus) {
      checkbox.focus();
    }
  }

  isChecked(): boolean | null {
    return this.model;
  }

  writeValue(model: any): void {
    // Normalize model (bool or null)
    if (typeof model == 'undefined' || model === '' || model === null) {
      this.model = null;
    } else {
      this.model = !!model;
    }

    this.checked = this.isChecked();

    this.cdr.markForCheck();
  }

  backgroundClass(hover, disabled, checked, focused) {
    return {
      'ui-state-hover'    : (hover && !disabled),
      'ui-state-active'   : (checked !== null),
      'ui-state-false'    : (checked === false),
      'ui-state-disabled' : disabled,
      'ui-state-focus'    : focused
    };
  }

  iconClass(checked: boolean | null) {
    return {
      'fa fal'       : true,
      'fa-fw'    : true,
      'fa-check' : (checked === true),
      'fa-times' : (checked === false)
    };
  }
}

@NgModule({
  imports      : [ CommonModule, CheckboxModule ],
  exports      : [ ThreeStateCheckbox ],
  declarations : [ ThreeStateCheckbox ]
})
export class ThreeStateCheckboxModule {
}