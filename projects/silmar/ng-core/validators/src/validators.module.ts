import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StringsModule } from '@silmar/ng-core';
import { FormErrorComponent } from './form-error.component';
import { RequiredIfValidator } from './required-if.validator';
import { RequiredIfEmptyValidator } from './required-if-empty.validator';
import { RequiredInputDirective } from './required-input.directive';
import { MaxValidator } from './max.validator';
import { MinValidator } from './min.validator';
import { VatValidator } from "./vat/vat.validator";


const DECLARATIONS = [
  FormErrorComponent,
  RequiredIfValidator,
  RequiredIfEmptyValidator,
  RequiredInputDirective,
  MaxValidator,
  MinValidator,
  VatValidator
];

@NgModule({
  imports      : [
    CommonModule,
    FormsModule,
    StringsModule
  ],
  declarations : DECLARATIONS,
  exports      : DECLARATIONS
})
export class ValidatorsModule {
}