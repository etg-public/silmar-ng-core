import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';
import { isString, isBlank } from '@silmar/ng-core';

const REQUIRED_IF_VALIDATOR_PROVIDER = {
  provide     : NG_VALIDATORS,
  useExisting : forwardRef(() => RequiredIfValidator),
  multi       : true
};

/**
 * Validate a required control if another one exists and is not empty.
 *
 * Do not forget the `reverse` attribute! It helps detecting changes on the reverse side
 * but still add errors to the non reverse control only.
 *
 * @example: `Phone` is REQUIRED if `extension` is preset:
 * <input type="text" name="phone" [(ngModel)]="user.phone" siRequiredIf="extension"/>
 * <input type="text" name="extension" [(ngModel)]="user.extension" siRequiredIf="phone" reverse="true"/>
 */
@Directive({
  selector  : '[siRequiredIf][formControlName],[siRequiredIf][formControl],[siRequiredIf][ngModel]',
  providers : [ REQUIRED_IF_VALIDATOR_PROVIDER ]
})
export class RequiredIfValidator implements Validator {
  /**
   * Validation error key
   */
  protected error: string = 'siRequiredIf';

  /**
   * @param otherElement
   * @param reverse
   */
  constructor(@Attribute('siRequiredIf') public otherElement: string,
              @Attribute('reverse') public reverse: string) {
  }

  /**
   * @inheritDoc
   */
  validate(control: AbstractControl): { [ key: string ]: any } {
    let otherControl = control.root.get(this.otherElement);

    // No other side - validation passes
    if (!otherControl) {
      return null;
    }

    let isValid = this.doValidate(control, otherControl);

    if (isValid) {
      // Remove other side's errors if in reverse control
      if (this.isReverse && otherControl.errors) {
        delete otherControl.errors[ this.error ];
        !Object.keys(otherControl.errors).length && otherControl.setErrors(null);
      }

      // Validation passes
      return null;
    }

    // Not valid
    let err           = {};
    err[ this.error ] = true;

    if (this.isReverse) {
      // Set error on other control
      otherControl.setErrors(err);

      // Validation passes for reversed control
      return null;
    }

    // Validation fails
    return err;
  }

  /**
   * Is it in the reversed control
   */
  protected get isReverse(): boolean {
    if (!this.reverse) {
      return false;
    }

    return this.reverse === 'true';
  }

  /**
   * Actual validation check
   * @param control
   * @param otherControl
   */
  protected doValidate(control: AbstractControl, otherControl): boolean {
    let value      = this.isReverse ? otherControl.value : control.value,
        otherValue = this.isReverse ? control.value : otherControl.value;

    if (otherControl && !isBlank(otherValue) && !(isString(otherValue) && otherValue == '')) {
      if (isBlank(value) || (isString(value) && value == '')) {
        return false;
      }
    }

    return true;
  }
}
