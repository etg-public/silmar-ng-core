import {Directive, ElementRef, forwardRef, Input, Renderer2} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator} from '@angular/forms';
import {isString, isNumber} from '@silmar/ng-core';

const MIN_VALIDATOR_PROVIDER = {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => MinValidator),
    multi: true
};

/**
 * Validate numeric min
 */
@Directive({
    selector: 'input[type=number][min][formControlName],input[type=number][min][formControl],input[type=number][min][ngModel]',
    providers: [MIN_VALIDATOR_PROVIDER]
})
export class MinValidator implements Validator {
    /**
     * Min number allowed
     */
    protected _min: number;

    /**
     * On validator change handler
     */
    protected _onChange: () => void;

    /**
     * @param elRef
     * @param renderer
     */
    constructor(protected elRef: ElementRef, protected renderer: Renderer2) {
    }

    /**
     * Set native element `min` prop and call onChange handler
     * @param value
     */
    @Input() set min(value) {
        if (isNaN(+value)) {
            return;
        }

        this._min = +value;
        this.renderer.setAttribute(this.elRef.nativeElement, 'min', value);

        // Validate after validation rule has changed
        this._onChange && this._onChange();
    }

    /**
     * @inheritDoc
     */
    validate(control: AbstractControl): { [key: string]: any } {
        let val = control.value;

        if (!isNumber(val) && (!isString(val) || isNaN(val = +val))) {
            return null;
        }

        if (val < this._min) {
            return {'min': {min: this._min}};
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    registerOnValidatorChange(fn: () => void): void {
        this._onChange = fn;
    }
}
