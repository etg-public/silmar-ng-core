import { AbstractControl, NG_VALIDATORS, Validator } from "@angular/forms";
import { Directive, forwardRef, Input } from "@angular/core";
import { getVatRegex } from "./vat-rules";

const VAT_VALIDATOR_PROVIDER = {
  provide     : NG_VALIDATORS,
  useExisting : forwardRef(() => VatValidator),
  multi       : true
};

/**
 * <input id="vat" name="vat" [vat]="iso_2letter" placeholder="VAT number" [(ngModel)]="object.vat" />
 */
@Directive({
  selector  : '[vat][formControlName],[vat][formControl],[vat][ngModel]',
  providers : [ VAT_VALIDATOR_PROVIDER ]
})
export class VatValidator implements Validator {
  @Input() set vat(countryCode: string) {
    this.countryCode = countryCode ? countryCode.toUpperCase() : null;
  }

  protected countryCode: string;
  /**
   * Validation error key
   */
  protected errorKey: string = 'vat';

  validate(control: AbstractControl): { [ key: string ]: any } {
    if (!control || !control.value || !this.countryCode) {
      return null;
    }

    let reg = getVatRegex(this.countryCode);

    if (!reg.length) {
      return null;
    }

    let value = control.value.trim().toUpperCase().replace(/[ \-.,:]/g, ''),
        res   = false;

    for (let r of reg) {
      if (r.test(value)) {
        res = true;
        break;
      }
    }

    return res ? null : { [ this.errorKey ] : { country : this.countryCode, vat : value } };
  }
}