/**
 * https://github.com/se-panfilov/jsvat
 * @param country
 */
export function getVatRegex(country: string): RegExp[] {
  const VATEXP = {
    AT  : [ /^(AT)U(\d{8})$/ ],
    BE  : [ /^(BE)(0?\d{9})$/ ],
    BG  : [ /^(BG)(\d{9,10})$/ ],
    CH  : [ /^(CH)E(\d{9})((MWST)|(IVA)|(TVA)|)$/ ],
    CY  : [ /^(CY)([0-5|9]\d{7}[A-Z])$/ ],
    CZ  : [ /^(CZ)(\d{8,10})(\d{3})?$/ ],
    DE  : [ /^(DE)([1-9]\d{8})$/ ],
    DK  : [ /^(DK)((\d{8}))$/ ],
    EE  : [ /^(EE)(10\d{7})$/ ],
    GR  : [ /^(EL)(\d{9})$/, /^(GR)(\d{8,9})$/ ],
    ES  : [
      /^(ES)([A-Z]\d{8})$/,
      /^(ES)([A-H|N-S|W]\d{7}[A-J])$/,
      /^(ES)([0-9|Y|Z]\d{7}[A-Z])$/,
      /^(ES)([K|L|M|X]\d{7}[A-Z])$/
    ],
    EU  : [ /^(EU)(\d{9})$/ ],
    FI  : [ /^(FI)(\d{8})$/ ],
    FR  : [
      /^(FR)(\d{11})$/,
      /^(FR)[(A-H)|(J-N)|(P-Z)]\d{10}$/,
      /^(FR)\d[(A-H)|(J-N)|(P-Z)]\d{9}$/,
      /^(FR)[(A-H)|(J-N)|(P-Z)]{2}\d{9}$/
    ],
    GB  : [
      /^(GB)?(\d{9})$/,
      /^(GB)?(\d{12})$/,
      /^(GB)?(GD\d{3})$/,
      /^(GB)?(HA\d{3})$/
    ],
    HR  : [ /^(HR)(\d{11})$/ ],
    HU  : [ /^(HU)(\d{8})$/ ],
    IE  : [ /^(IE)(\d{7}[A-W])$/, /^(IE)(\d{7}[A-W][AH])$/, /^(IE)([7-9][A-Z*+)]\d{5}[A-W])$/ ],
    IT  : [ /^(IT)(\d{11})$/ ],
    LV  : [ /^(LV)(\d{11})$/ ],
    LT  : [ /^(LT)(\d{9}|\d{12})$/ ],
    LU  : [ /^(LU)(\d{8})$/ ],
    MT  : [ /^(MT)([1-9]\d{7})$/ ],
    NL  : [ /^(NL)(\d{9})B\d{2}$/ ],
    PL  : [ /^(PL)(\d{10})$/ ],
    PT  : [ /^(PT)(\d{9})$/ ],
    RO  : [ /^(RO)([1-9]\d{1,9})$/ ],
    SI  : [ /^(SI)([1-9]\d{7})$/ ],
    SK  : [ /^(SK)([1-9]\d[(2-4)|(6-9)]\d{7})$/ ],
    SE  : [ /^(SE)(\d{10}01)$/ ],
    ANY : [ /^[^.\-:/]+$/ ]
  };

  const ISO_TRANSLATE = {
    GF : 'FR',
    PF : 'FR',
    TF : 'FR'
  };

  country = country.toUpperCase();
  country = ISO_TRANSLATE.hasOwnProperty(country) ? ISO_TRANSLATE[ country ] : country;

  if (VATEXP.hasOwnProperty(country)) {
    return VATEXP[ country ];
  }

  return [];
}
