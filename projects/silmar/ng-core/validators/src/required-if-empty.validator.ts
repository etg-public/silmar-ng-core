import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';
import { RequiredIfValidator } from './required-if.validator';
import { isString, isBlank } from '@silmar/ng-core';

const REQUIRED_IF_EMPTY_VALIDATOR_PROVIDER = {
  provide     : NG_VALIDATORS,
  useExisting : forwardRef(() => RequiredIfEmptyValidator),
  multi       : true
};

/**
 * Validate a required control if another one not exists or is empty.
 *
 * Do not forget the `reverse` attribute! It helps detecting changes on the reverse side
 * but still add errors to the non reverse control only.
 *
 * @example: `Phone` is REQUIRED if `email` is empty:
 * <input type="text" name="phone" [(ngModel)]="user.phone" siRequiredIfEmpty="email"/>
 * <input type="text" name="email" [(ngModel)]="user.email" siRequiredIfEmpty="phone" reverse="true"/>
 */
@Directive({
  selector  : '[siRequiredIfEmpty][formControlName],[siRequiredIfEmpty][formControl],[siRequiredIfEmpty][ngModel]',
  providers : [ REQUIRED_IF_EMPTY_VALIDATOR_PROVIDER ]
})
export class RequiredIfEmptyValidator extends RequiredIfValidator implements Validator {
  /**
   * Validation error key
   */
  protected error: string = 'siRequiredIfEmpty';

  /**
   * @param otherElement
   * @param reverse
   */
  constructor(@Attribute('siRequiredIfEmpty') public otherElement: string,
              @Attribute('reverse') public reverse: string) {
    super(otherElement, reverse);
  }

  /**
   * @inheritDoc
   */
  validate(control: AbstractControl): { [ key: string ]: any } {
    let otherControl = control.root.get(this.otherElement);

    // No other side - validation fails
    if (!otherControl) {
      let err           = {};
      err[ this.error ] = true;

      return err;
    }

    return super.validate(control);
  }

  /**
   * Actual validation check
   * @param control
   * @param otherControl
   */
  protected doValidate(control: AbstractControl, otherControl): boolean {
    let value      = this.isReverse ? otherControl.value : control.value,
        otherValue = this.isReverse ? control.value : otherControl.value;

    if (!otherControl || isBlank(otherValue) || (isString(otherValue) && otherValue == '')) {
      if (isBlank(value) || (isString(value) && value == '')) {
        return false;
      }
    }

    return true;
  }
}
