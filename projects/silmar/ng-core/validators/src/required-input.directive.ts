import { AfterViewInit, Directive, ElementRef, Inject, Input, PLATFORM_ID, Renderer2 } from '@angular/core';
import { NgModel } from '@angular/forms';
import { isDefined } from '@silmar/ng-core';
import { isPlatformBrowser } from '@angular/common';

/**
 * Override of `required` directive
 *
 * Add `*` symbol on corresponding label and change color if invalid
 */
@Directive({
  selector  : '[id][required][ngModel]:not([formControlName]):not([formControl]):not([noLabel])',
  providers : [ NgModel ]
})
export class RequiredInputDirective implements AfterViewInit {
  /**
   * Use this css class for labels of required inputs
   */
  @Input() requiredClass = 'required';

  /**
   * Use this css class for labels of invalid inputs
   */
  @Input() invalidClass = 'invalid';

  /**
   * Element id
   */
  protected elId: string;

  /**
   * The input is required
   */
  protected isRequired: boolean;

  /**
   * The label attached to the input
   */
  protected attachedLabel: any;

  /**
   * State of the required css class
   */
  protected reqClassIsSet: boolean = false;

  /**
   * State of the invalid css class
   */
  protected invClassIsSet: boolean = false;

  /**
   * Last known status of the input
   */
  protected status: string;

  /**
   * platform check
   */
  protected isBrowser: boolean = true;

  /**
   * @param platformId
   * @param renderer
   * @param ngModel
   * @param elRef
   */
  constructor(@Inject(PLATFORM_ID) platformId, protected renderer: Renderer2, protected ngModel: NgModel, protected elRef: ElementRef) {
    this.elId      = elRef.nativeElement.id;
    this.isBrowser = isPlatformBrowser(platformId);
  }

  /**
   * Set the required validator of the input element
   * @param value
   */
  @Input() set required(value) {
    if (this.isRequired !== value) {
      this.isRequired = value;
      this.handleRequiredClass();
    }
  }

  /**
   * Explicitly attach a label to the input
   * @param value
   */
  @Input() set label(value) {
    if (this.attachedLabel != value) {
      this.attachedLabel = value;
      this.handleRequiredClass();
    }
  }

  /**
   * @inheritDoc
   */
  ngAfterViewInit() {
    if (!this.elId) {
      this.elId = this.elRef.nativeElement.id;
    }

    setTimeout(() => {
      this.getLabel();
      this.handleRequiredClass().handleInvalidClass();

      this.ngModel.statusChanges.subscribe((status) => {
        if (this.status != status) {
          this.handleInvalidClass().status = status;
        }
      });
    });
  }

  /**
   * Set or remove the required css class to the label
   */
  protected handleRequiredClass(): this {
    let isRequired = isDefined(this.isRequired) && this.isRequired !== false;

    if (isRequired) {
      // if the required class is not yet set and we have label, set the required class
      !this.reqClassIsSet && this.getLabel() && (this.reqClassIsSet = true) && this.renderer.addClass(
        this.attachedLabel, this.requiredClass
      );
    } else {
      // if the required class is set and we have label, remove the required class
      this.reqClassIsSet && this.getLabel() && !(this.reqClassIsSet = false) && this.renderer.removeClass(
        this.attachedLabel, this.requiredClass
      );
    }

    return this;
  }

  /**
   * Set or remove the invalid css class
   */
  protected handleInvalidClass(): this {
    if (!this.invalidClass) {
      return this;
    }

    let isValid = this.ngModel.isDisabled ? true : this.ngModel.valid;

    if (!isValid) {
      // if the invalid class is not yet set and we have label, set the invalid class
      !this.invClassIsSet && this.getLabel() && (this.invClassIsSet = true) && this.renderer.addClass(
        this.attachedLabel, this.invalidClass
      );
    } else {
      // if the invalid class is set and we have label, remove the invalid class
      this.invClassIsSet && this.getLabel() && !(this.invClassIsSet = false) && this.renderer.removeClass(
        this.attachedLabel, this.invalidClass
      );
    }

    return this;
  }

  /**
   * Get the attached label
   */
  protected getLabel(): any {
    if (!this.isBrowser || !this.elId) {
      return false;
    }

    if (this.attachedLabel) {
      return this.attachedLabel;
    }

    //noinspection TypeScriptValidateTypes
    this.attachedLabel = document.querySelector('label[for=' + this.elId + ']');

    return this.attachedLabel;
  }
}
