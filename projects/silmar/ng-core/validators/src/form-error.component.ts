import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Subscription } from 'rxjs';

/**
 * Build form errors
 *
 * ```html
 *  <si-form-error [input]="name"
 *    [messages]="{'required':'This field is required!','minlength':'Enter at least :requiredLength characters'}">
 *  </si-form-error>
 *
 *  <si-form-error [input]="name" [messages]="{'*':'Invalid field'}"></si-form-error>
 *
 *  <si-form-error [input]="name" [message]="'Invalid field'"></si-form-error>
 *  <!-- This is equal to: -->
 *  <si-form-error [input]="name" [message]="{'*':'Invalid field'}" [ignoreDefault]="true" ></si-form-error>
 * ```
 */
@Component({
  selector        : 'si-form-error',
  template        : `
    <ng-container *ngIf="!valid && (!ignorePristine || dirty || hasValue) && !disabled">
      <div *ngIf="!plain; else plainTpl" class="form-error invalid-feedback">
        <ul class="invalid-feedback-ul">
          <li *ngFor="let errKey of errorKeys">
            <i [class]="iconClass" *ngIf="iconClass"></i>&nbsp;{{(errMessages[errKey] || errMessages['*']) | sprintf:valErrors[errKey]}}
          </li>
        </ul>
      </div>
      <ng-template #plainTpl>
        <ng-container *ngFor="let errKey of errorKeys; let last=last">
          {{(errMessages[errKey] || errMessages['*']) | sprintf:valErrors[errKey]}}
          <ng-container *ngIf="!last">, </ng-container>
        </ng-container>
      </ng-template>
    </ng-container>
  `,
  styles          : [
      `
          .invalid-feedback {
              color: #e62a10;
              font-size: .85em;
          }

          .invalid-feedback-ul {
              display: block;
              margin: 0 auto;
              padding: 0;
              list-style-type: none;
          }
    `
  ],
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class FormErrorComponent implements OnDestroy {
  /**
   * Is control valid
   */
  valid: boolean;

  /**
   * Is control dirty
   */
  dirty: boolean;

  /**
   * Is control disabled
   */
  disabled: boolean;

  /**
   * Control validation erros
   */
  valErrors: { [ key: string ]: any; };

  /**
   * The control has value entered
   */
  hasValue: boolean;

  /**
   * Error messages
   */
  errMessages: { [ key: string ]: any } = {};

  /**
   *
   */
  errorKeys: string[];

  /**
   * Control status change observable
   */
  protected controlStatusChange$: Subscription;

  /**
   *
   */
  protected formSubmitChange$: Subscription;

  /**
   * Control status
   */
  protected status: string;

  /**
   * Default error messages
   *
   */
  protected _defaultErrors = {
    'required'          : 'Please, enter value',
    'minlength'         : 'Enter at least :requiredLength characters',
    'maxlength'         : 'Enter at most :requiredLength characters',
    'pattern'           : 'Entered value does not match required format',
    'siRequiredIf'      : 'Please, enter value',
    'siRequiredIfEmpty' : 'Please, enter value',
    'bsValidateEntity'  : 'Entered value is not an entity',
    'bsPaxCount'        : 'Enter equal number of pax per room',
    'bsPhoneNumber'     : '\':phone\' is invalid phone number for :code',
    'bsCountryCode'     : ':code is not known country code',
    'siValidateEntity'  : 'Entered value is not an entity',
    'siPaxCount'        : 'Enter equal number of pax per room',
    'siPhoneNumber'     : '\':phone\' is invalid phone number for :code',
    'siCountryCode'     : ':code is not known country code',
    'email'             : 'Please, enter valid email',
    'min'               : 'Value must be minimum :min',
    'minError'          : 'Value must be minimum :minValue',
    'max'               : 'Value must be maximum :max',
    'maxError'          : 'Value must be maximum :maxValue',
    'date'              : '\':date\' is not a valid date',
    'time'              : '\':time\' is not a valid time',
    'maxDate'           : 'Selected date must not be after :date',
    'minDate'           : 'Selected date must not be before :date',
    'notWeekend'        : 'Selected date must not be on a weekend',
    'vat'               : 'Invalid VAT number for :country',
    '*'                 : 'Invalid value'
  };

  /**
   * Ignore default messages and use only custom
   *
   */
  @Input() ignoreDefault: boolean = false;

  /**
   * Set custom warning icon class or disable it (set to false)
   */
  @Input() iconClass: string | false = 'fa fal fa-exclamation-triangle';

  /**
   * Ignore pristine (not dirty fields)
   *
   */
  @Input() ignorePristine: boolean = true;

  @Input() plain: boolean = false;

  constructor(protected cd: ChangeDetectorRef) {
    this.errMessages = this._defaultErrors;
  }

  /**
   * Placeholder for the AutoUnsubscribe
   */
  ngOnDestroy() {
    this.controlStatusChange$ && this.controlStatusChange$.unsubscribe();
    this.formSubmitChange$ && this.formSubmitChange$.unsubscribe();
  }

  /**
   * Subscribe for control's status (VALID, INVALID, etc...) change
   *
   */
  @Input()
  set input(control: NgModel) {
    if (control.formDirective && control.formDirective.ngSubmit) {
      this.formSubmitChange$ = control.formDirective.ngSubmit.subscribe(_ => {
        if (!this.disabled && !this.dirty) {
          this.dirty = control.formDirective.submitted;

          this.cd.markForCheck();
        }
      });
    }

    this.controlStatusChange$ = control.statusChanges.subscribe(status => {
      this.status = status;
      this.onStatusChange(control);
    });
  }

  /**
   * Use the given error message for all errors
   *
   */
  @Input()
  set message(message: string) {
    this.errMessages = {};
    this.messages    = { '*' : message };
  }

  /**
   * Set error messages, the given messages will get merged with the defaults unless [ignoreDefault] is true
   *
   */
  @Input()
  set messages(errors: { [ key: string ]: any }) {
    if (this.ignoreDefault) {
      this.errMessages = errors;
    } else {
      for (let key of Object.keys(errors)) {
        this.errMessages[ key ] = errors[ key ];
      }
    }
  }

  /**
   *
   */
  get messages() {
    return this.errMessages;
  }

  /**
   * Clone it's state and update UI
   *
   */
  protected onStatusChange(control: NgModel) {
    this.valid     = control.valid;
    this.disabled  = control.disabled;
    this.dirty     = control.dirty;
    this.valErrors = control.errors;
    this.errorKeys = control.errors ? Object.keys(control.errors) : [];
    this.hasValue  = control.value != null && control.value.hasOwnProperty('length')
      ? control.value.length > 0
      : !!control.value;

    this.cd.markForCheck();
  }
}
