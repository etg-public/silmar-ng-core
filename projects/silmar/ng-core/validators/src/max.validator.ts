import { Directive, ElementRef, forwardRef, Input, Renderer2 } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { isString, isNumber } from '@silmar/ng-core';

const MAX_VALIDATOR_PROVIDER = {
  provide     : NG_VALIDATORS,
  useExisting : forwardRef(() => MaxValidator),
  multi       : true
};

/**
 * Validate numeric max
 */
@Directive({
  selector  : 'input[type=number][max][formControlName],input[type=number][max][formControl],input[type=number][max][ngModel]',
  providers : [ MAX_VALIDATOR_PROVIDER ]
})
export class MaxValidator implements Validator {
  /**
   * Max number allowed
   */
  protected _max: number;

  /**
   * On validator change handler
   */
  protected _onChange: () => void;

  /**
   * @param elRef
   * @param renderer
   */
  constructor(protected elRef: ElementRef, protected renderer: Renderer2) {
  }

  /**
   * Set native element `max` prop and call onChange handler
   * @param value
   */
  @Input()
  set max(value) {
    if (isNaN(+value)) {
      return;
    }

    this._max = +value;
    this.renderer.setAttribute(this.elRef.nativeElement, 'max', value);

    // Validate after validation rule has changed
    this._onChange && this._onChange();
  }

  /**
   * @inheritDoc
   */
  validate(control: AbstractControl): { [ key: string ]: any } {
    let val = control.value;

    if (!isNumber(val) && (!isString(val) || isNaN(val = +val))) {
      return null;
    }

    if (val > this._max) {
      return { 'max' : { max : this._max } };
    }

    return null;
  }

  /**
   * @inheritDoc
   */
  registerOnValidatorChange(fn: () => void): void {
    this._onChange = fn;
  }
}
