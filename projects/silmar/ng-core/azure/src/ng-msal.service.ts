import { Inject, Injectable, Optional } from '@angular/core';
import { MSAL_CONFIG, MsalConfig } from './ng-msal-config';
import { AuthenticationParameters, AuthResponse, Configuration, UserAgentApplication } from 'msal';
import { isDefined, LogService, Logger } from '@silmar/ng-core';
import { from, Observable, throwError } from 'rxjs';

@Injectable({ providedIn : 'root' })
export class NgMsalService {
  protected msal: UserAgentApplication;
  protected logger: Logger | Console;

  constructor(@Inject(MSAL_CONFIG) protected config: MsalConfig, @Optional() logSvc: LogService) {
    const cfg: Configuration = { auth : { clientId : config.clientId } };

    isDefined(config.authority) && (cfg.auth.authority = config.authority);
    isDefined(config.validateAuthority) && (cfg.auth.validateAuthority = config.validateAuthority);
    isDefined(config.redirectUri) && (cfg.auth.redirectUri = config.redirectUri);
    isDefined(config.postLogoutRedirectUri) && (cfg.auth.postLogoutRedirectUri = config.postLogoutRedirectUri);
    isDefined(config.loadFrameTimeout) && (cfg.system.loadFrameTimeout = config.loadFrameTimeout);

    this.msal = new UserAgentApplication(cfg);

    this.logger = logSvc ? logSvc.to('msal') : console;
  }

  login(request: AuthenticationParameters = {}): Observable<AuthResponse> {
    if (this.config.popUp) {
      return this.loginPopup(request);
    }

    return this.loginRedirect(request);
  }

  loginRedirect(request: AuthenticationParameters = {}): Observable<any> {
    return throwError('Not implemented');
  }

  loginPopup(request: AuthenticationParameters = {}): Observable<AuthResponse> {
    this.logger.debug('Login popup flow');
    return from(this.msal.loginPopup(request));
  }

  logout(): void {
    this.msal.logout();
  }
}
