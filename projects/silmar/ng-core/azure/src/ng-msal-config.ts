import { InjectionToken } from '@angular/core';

export const MSAL_CONFIG = new InjectionToken<MsalConfig>('silmar-msal-module MSAL_CONFIG');

export class MsalConfig {
  clientId: string;
  authority ?         = 'https://login.microsoftonline.com/common';
  validateAuthority ? = true;
  redirectUri?: string;
  postLogoutRedirectUri?: string;
  loadFrameTimeout?   = 6000;
  popUp?: boolean     = true;
}
