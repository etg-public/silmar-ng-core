import { InjectionToken } from '@angular/core';

export interface UrlConfig {
  catchAuxClick: boolean;
}

export const URL_CONFIG = new InjectionToken<UrlConfig>('silmar-url-module URL_CONFIG');
