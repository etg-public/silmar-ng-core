import { Inject, Injectable, Optional } from '@angular/core';
import { urlQueryString, Logger, LogService } from '@silmar/ng-core';
import { URL_ROUTES, UrlRoutes } from './url-models';

/**
 * Builds internal url by route name & params
 */
@Injectable({ providedIn : 'root' })
export class UrlBuilder {
  /**
   * Logger
   */
  protected logger: Logger | Console;

  constructor(@Inject(URL_ROUTES) protected routes: UrlRoutes, @Optional() loggerSvc: LogService = null) {
    this.logger = loggerSvc ? loggerSvc.to('url-builder') : console;
  }

  /**
   * Build an url
   */
  url(name: string, params: { [ key: string ]: any } = {}, query = {}): string {
    // Find path and route group by route name.
    // Extract url params from path.
    try {
      let { path, group } = this.parseName(name);

      let parsedParams = this.extractParams(path);

      // Replace url params
      path = this.replaceParams(path, parsedParams, params);

      // Add query params
      let tmpParams = urlQueryString(query, true);
      path          = path + (tmpParams ? tmpParams : '');

      // Prepend with route group
      return (group ? ('/' + group) : '') + '/' + path;
    } catch (e) {
      this.logger.error('Can find route named ' + name);

      return '';
    }
  }

  /**
   * Find route path & group by route name
   */
  protected parseName(name: string): { path: string, group: string } {
    let names = name.split('.'),
        key, path, group;

    if (names.length == 2) {
      // Name has a CRUD suffix
      key = names[ 0 ];

      if (!this.routes.hasOwnProperty(key) || !this.routes[ key ].hasOwnProperty('crud')) {
        throw new Error('Bad route name');
      }

      path = this.routes[ key ].crud[ names[ 1 ] ];
    } else {
      // Name has no CRUD suffix
      key = name;

      if (!this.routes.hasOwnProperty(key) || !this.routes[ key ].hasOwnProperty('path')) {
        throw new Error('Bad route name');
      }

      path = this.routes[ key ].path;
    }

    // Route group
    group = this.routes[ key ].group || '';

    return { path, group };
  }

  /**
   * Extract url params
   */
  protected extractParams(path: string) {
    let match,
        pattern = /\/?(:\w+)\/?/g,
        params  = [];

    while (match = pattern.exec(path)) {
      params.push(match[ 1 ]);
    }

    return params;
  }

  /**
   * Replace url params
   *
   * @return string
   */
  protected replaceParams(path: string, parsedParams: any[], params: { [ p: string ]: string }): string {
    let paramName;

    for (let param of parsedParams) {
      // Remove `:` prefix
      paramName = param.substr(1);

      // Missing url param log an error and stop replacing process
      if (!params.hasOwnProperty(paramName)) {
        this.logger.error('Not all required url parameters given', path, params);

        return '';
      }

      // Replace param
      path = path.replace(param, params[ paramName ]);
    }

    return path;
  }
}
