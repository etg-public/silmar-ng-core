import { ModuleWithProviders, NgModule } from '@angular/core';
import { URL_ROUTES, UrlRoutes } from './url-models';
import { UrlDirective } from './url.directive';

@NgModule({
  exports      : [ UrlDirective ],
  declarations : [ UrlDirective ],
})
export class UrlModule {
  static forRoot(routes: UrlRoutes): ModuleWithProviders<UrlModule> {
    return {
      ngModule  : UrlModule,
      providers : [
        { provide : URL_ROUTES, useValue : routes }
      ]
    };
  }
}
