import { InjectionToken } from '@angular/core';

/**
 * Route interface
 */
export interface UrlRoute {
  /**
   * Path that will be appended to route group
   */
  path: string,

  /**
   * Lazy loaded route prefix
   */
  group?: string,

  /**
   * Shorthand for generic route types
   */
  crud?: {
    create?: string,
    read?: string,
    update?: string,
  }
}

/**
 * Routes
 */
export interface UrlRoutes {
  [ key: string ]: UrlRoute
}

export const DEFAULT_CRUD_SUB_PATHS = {
  create : 'add',
  read   : ':id',
  update : ':id/edit'
};

export const URL_ROUTES = new InjectionToken<UrlRoutes>('url.routes');
