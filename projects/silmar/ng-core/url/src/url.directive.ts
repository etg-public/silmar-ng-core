import {
  AfterViewInit,
  Directive,
  ElementRef,
  HostListener,
  Inject,
  Input,
  OnChanges,
  Optional,
  Renderer2,
  SimpleChanges
} from '@angular/core';
import { Router } from '@angular/router';
import { UrlBuilder } from './url-builder.service';
import { URL_CONFIG, UrlConfig } from './url.config';

/**
 * Internal link by route name directive
 *
 * <a [siUrl]="'advance-bookings'" [bsUrlParams]="{id: 1}" [bsUrlQuery]="{expanded_id: 2}">Advance bookings</a>
 */
@Directive({
  selector : '[siUrl]',
})
export class UrlDirective implements AfterViewInit, OnChanges {
  /**
   * Route name
   */
  @Input('siUrl') path: string;

  /**
   * Route params
   */
  @Input('siUrlParams') params: { [ key: string ]: any } = {};

  /**
   * Query params
   */
  @Input('siUrlQuery') qParams: { [ key: string ]: any } = {};

  /**
   * Link style class
   */
  @Input('siLinkClass') linkClass: string = 'colored';

  protected url: string;

  protected catchAuxClick: boolean = true;

  constructor(protected urlSvc: UrlBuilder,
              protected router: Router,
              protected renderer: Renderer2,
              protected el: ElementRef,
              @Optional() @Inject(URL_CONFIG) cfg: UrlConfig) {
    this.catchAuxClick = cfg && cfg.hasOwnProperty('catchAuxClick') ? !!cfg.catchAuxClick : true;
  }

  ngAfterViewInit(): void {
    this.renderer.setStyle(this.el.nativeElement, 'cursor', 'pointer');
    this.renderer.addClass(this.el.nativeElement, this.linkClass);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes[ 'path' ] || changes[ 'qParams' ] || changes[ 'params' ]) {
      this.url = this.urlSvc.url(this.path, this.params, this.qParams);

      if (this.el.nativeElement.tagName == 'A') {
        this.renderer.setProperty(this.el.nativeElement, 'href', this.url);
      }
    }
  }

  /**
   * Navigate on link click
   */
  @HostListener('click', [ '$event' ])
  onClick(e: MouseEvent) {
    if (!e.ctrlKey) {
      this.navigate();

      e.preventDefault();
    }
  }

  /**
   * Navigate on middle button click
   *
   * Experimental: Works on Chrome only
   *
   * [AuxClick MDN]{@link https://developer.mozilla.org/en-US/docs/Web/Events/auxclick}
   */
  @HostListener('auxclick', [ '$event' ])
  onAuxClick(e: MouseEvent) {
    if (this.catchAuxClick && e && ((e.which && e.which == 2) || (e.button == 1))) {
      this.navigate();

      e.preventDefault();
    }
  }

  /**
   */
  protected navigate() {
    if (!this.url) {
      return;
    }

    this.router.navigateByUrl(this.url);
  }
}
