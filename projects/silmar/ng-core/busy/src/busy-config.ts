import { InjectionToken } from '@angular/core';

/**
 *
 */
export interface BusyConfigInterface {
  minDuration?: number;
  backdrop?: boolean;
  message?: string;
  wrapperClass?: string;
  spinner?: 'orbit'|'fingerprint';
}

export class BusyConfig implements BusyConfigInterface {
  minDuration: number;
  backdrop: boolean;
  message: string;
  wrapperClass: string;
  spinner: 'orbit'|'fingerprint'

  constructor(config: BusyConfigInterface = {}) {
    Object.assign(this, BUSY_CONFIG_DEFAULTS, config);
  }
}

export const BUSY_CONFIG_DEFAULTS = {
  minDuration        : 100,
  backdrop           : true,
  message            : 'Please wait ',
  wrapperClass       : 'ng-busy',
  spinner            : 'orbit'
};

export let BUSY_CONFIG = new InjectionToken<BusyConfigInterface>('busy.config');
