import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from "@angular/core";
import { PromiseTrackerService } from "./promise-tracker.service";
import { BusyService } from "./busy.service";
import { Subscription } from "rxjs";

@Component({
  template        : `
    <div class="ng-busy-backdrop" *ngIf="backdrop && isActive"></div>
    <div class="{{wrapperClass}}" *ngIf="isActive">
      <div class="ng-busy-default-wrapper">
        <div class="ng-busy-default-sign">
          <ng-container [ngSwitch]="spinner">
            <si-busy-spinner-orbit *ngSwitchCase="'orbit'"></si-busy-spinner-orbit>
            <si-busy-spinner-fingerprint *ngSwitchCase="'fingerprint'"></si-busy-spinner-fingerprint>
          </ng-container>
          <span [textContent]="message"></span>
        </div>
      </div>
    </div>
  `,
  styleUrls       : [ 'busy.component.css' ],
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class BusyComponent implements OnDestroy {
  backdrop: boolean;

  message: string;

  wrapperClass: string;

  isActive: boolean;

  spinner: "orbit" | "fingerprint";

  protected track$: Subscription;

  constructor(tracker: PromiseTrackerService, service: BusyService, cd: ChangeDetectorRef) {
    this.backdrop     = service.config.backdrop;
    this.message      = service.config.message;
    this.wrapperClass = service.config.wrapperClass;
    this.spinner      = service.config.spinner;

    this.track$ = tracker.isActive()
      .subscribe((active: boolean) => {
        this.isActive = active;
        cd.markForCheck();
      });
  }

  ngOnDestroy() {
    this.track$ && this.track$.unsubscribe();
  }
}
