import { Injectable } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';

@Injectable()
export class PromiseTrackerService {
  promiseList: Array<Promise<any> | Subscription> = [];
  durationPromise: number | any;
  minDuration: number;
  onFinish: Function;
  subject: Subject<boolean> = new Subject<boolean>();

  reset(options: IPromiseTrackerOptions) {
    this.minDuration = options.minDuration;
    this.promiseList = [];

    options.promiseList.forEach(promise => {
      if (!promise || promise[ 'busyFulfilled' ]) {
        return;
      }
      this.addPromise(promise);
    });

    if (this.promiseList.length > 0) {
      if (options.onFinish) {
        this.onFinish = options.onFinish;
      }

      if (options.minDuration) {
        this.durationPromise = setTimeout(
          () => {
            this.durationPromise = null;
            this.subject.next(this._isActive());
          },
          options.minDuration
        );
      }
    }

    this.subject.next(this._isActive());
  }

  private addPromise(promise: Promise<any> | Subscription) {
    if (!promise || promise[ 'busyFulfilled' ] || this.promiseList.indexOf(promise) !== -1) {
      return;
    }

    if (promise instanceof Promise) {
      promise.then.call(
        promise,
        () => this.finishPromise(promise),
        () => this.finishPromise(promise)
      );
    } else if (promise instanceof Subscription && !promise.closed) {
      promise.add(() => setTimeout(() => this.finishPromise(promise), 100));
    } else {
      return;
    }

    !promise[ 'busyFulfilled' ] && this.promiseList.push(promise);

    this.subject.next(this._isActive());
  }

  private finishPromise(promise: Promise<any> | Subscription) {
    promise[ 'busyFulfilled' ] = true;
    const index                = this.promiseList.indexOf(promise);

    if (index === -1) {
      return;
    }

    this.promiseList.splice(index, 1);
    this.onFinish && this.onFinish();

    this.subject.next(this._isActive());
  }

  isActive(): Observable<any> {
    return this.subject.asObservable();
  }

  _isActive() {
    return this.durationPromise || this.promiseList.length > 0;
  }
}

export interface IPromiseTrackerOptions {
  minDuration?: number;
  promiseList: Promise<any>[];
  onFinish?: Function;
}
