import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusyComponent } from './busy.component';
import { BusyDirective } from './busy.directive';
import { BUSY_CONFIG, BusyConfigInterface } from './busy-config';
import {BusySpinnerOrbitComponent} from "./spinners/busy-spinner-orbit.component";
import {BusySpinnerFingerprintComponent} from "./spinners/busy-spinner-fingerprint.component";

@NgModule({
    imports: [CommonModule],
    exports: [BusyComponent, BusyDirective],
    declarations: [BusyComponent, BusyDirective, BusySpinnerOrbitComponent, BusySpinnerFingerprintComponent]
})
export class BusyModule {
  static forRoot(config?: BusyConfigInterface): ModuleWithProviders<BusyModule> {
    return {
      ngModule  : BusyModule,
      providers : [
        { provide : BUSY_CONFIG, useValue : config },
      ]
    };
  }
}
