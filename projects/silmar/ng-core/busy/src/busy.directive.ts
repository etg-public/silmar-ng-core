import {
  ComponentRef,
  Directive,
  EventEmitter, Injector,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewContainerRef
} from "@angular/core";
import {Subscription} from "rxjs";
import {PromiseTrackerService} from "./promise-tracker.service";
import {BusyService} from "./busy.service";
import {BusyConfigInterface} from "./busy-config";
import {BusyComponent} from "./busy.component";
import {equals} from '@silmar/ng-core';

@Directive({
  selector  : '[siBusy]',
  providers : [ PromiseTrackerService ]
})
export class BusyDirective implements OnChanges {
  protected busy;
  protected config: BusyConfigInterface;
  protected optionsRecord: any = {};
  protected busyRef: ComponentRef<BusyComponent>;

  @Output() onBusyFinish = new EventEmitter;

  constructor(private busyService: BusyService,
              private tracker: PromiseTrackerService,
              private injector: Injector,
              private _viewContainerRef: ViewContainerRef) {

    this.config = this.busyService.config;
    this.updateConfig();
    this.tracker.onFinish = () => {
      this.busyRef && this.busyRef.changeDetectorRef.markForCheck();
      this.onBusyFinish.emit();
    };
  }

  @Input('siBusy') set siBusy(busy: Promise<any> | Subscription | Array<Promise<any> | Subscription>) {
    this.busy = !Array.isArray(busy) ? [ busy ] : busy;
  }

  @Input('siBusyOptions') set siBusyOptions(options) {
    this.config = Object.assign({}, this.busyService.config, options);

    if (this.config.message) {
      this.config.message = this.config.message.replace('...', '').trim() + ' ';
    }
  }

  detectOptionsChange() {
    if (equals(this.config, this.optionsRecord)) {
      return false;
    }

    this.optionsRecord = this.config;
    return true;
  }

  ngOnChanges(change: SimpleChanges) {
    change[ 'siBusyOptions' ] && this.updateConfig();
    change[ 'siBusy' ] && this.updateTracker();
  }

  updateTracker() {
    if (this.busy.length == this.tracker.promiseList.length) {
      let equal = true;

      for (let item of this.busy) {
        if (this.tracker.promiseList.indexOf(item) === -1) {
          equal = false;
          break;
        }
      }

      if (equal) {
        return false;
      }
    }

    this.tracker.reset({
      promiseList : this.busy,
      minDuration : this.config.minDuration
    });

    return true;
  }

  updateConfig() {
    if (!this.detectOptionsChange()) {
      return false;
    }

    if (!this.busyRef) {
      this.loadBusy();
    } else {
      this.configureComponent();
    }

    return true;
  }

  private loadBusy() {
    this.busyRef && this.busyRef.destroy();

    this.busyRef                  = this._viewContainerRef.createComponent(BusyComponent, {injector: this.injector});
    this.configureComponent();
  }

  private configureComponent() {
    for (let key of Object.keys(this.config)) {
      if (this.busyRef.instance.hasOwnProperty(key) && this.busyRef.instance[ key ] != this.config[ key ]) {
        this.busyRef.instance[ key ] = this.config[ key ];
      }
    }

    this.busyRef.changeDetectorRef.markForCheck();
  }
}
