import { Inject, Injectable, Optional } from '@angular/core';
import { BusyConfigInterface, BusyConfig, BUSY_CONFIG } from './busy-config';

@Injectable({ providedIn : 'root' })
export class BusyService {
  private _config: BusyConfigInterface;

  constructor(@Optional() @Inject(BUSY_CONFIG) config: BusyConfigInterface = null) {
    this._config = new BusyConfig(config);
  }

  set config(config: BusyConfigInterface) {
    this._config = new BusyConfig(config);
  }

  get config(): BusyConfigInterface {
    return this._config;
  }
}
