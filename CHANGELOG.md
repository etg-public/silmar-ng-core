# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [5.0.0] - 2020-07-14
- Angular 10 

## [4.0.0] - 2020-04-02
- Angular 9
